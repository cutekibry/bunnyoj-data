### 题目描述
你需要回答十种基础博弈论问题，每种问题中都有两名玩家 Alice 和 Bob， Alice 先手。给定游戏规则，请你计算在两位玩家都足够聪明的情况下，该游戏是先手必胜还是先手必败。若先手必胜，输出 `Alice`，否则输出 `Bob`。

问题类型如下：

问题 $1$：有 $n$ 个石头堆，第 $i$ 个石头堆有 $A_i$ 个石头。Alice 和 Bob 轮流选定一个非空的石头堆，从中取走任意正整数个石头，不能取者失败。

问题 $2$：有 $n$ 个石头堆，第 $i$ 个石头堆有 $A_i$ 个石头。Alice 和 Bob 轮流选定一个非空的石头堆，从中取走任意正整数个石头，不能取者**胜利**。

问题 $3$：有一个石头堆，有 $n$ 个石头。Alice 和 Bob 轮流从中取走任意不超过 $m$ 的正整数个石头，不能取者**失败**。

问题 $4$：有一个石头堆，有 $n$ 个石头。Alice 和 Bob 轮流从中取走任意不超过 $m$ 的正整数个石头，不能取者**失败**。


问题 $5$：有 $n$ 个石头堆，第 $i$ 个石头堆有 $A_i$ 个石头。Alice 和 Bob 轮流选定一个非空的石头堆，从中将任意正整数个石头移到编号比它小一的石头堆（**特别地，若选中的编号为 $1$，则将这些石头丢弃**），不能取者失败。

