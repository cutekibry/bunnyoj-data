#include <cstdio>

const int MAXN = 3000 + 10;
const int MOD = 1000000009;

char s[MAXN];
int f[MAXN], s0[MAXN], s1[MAXN];

int main() {
    int i, j, k;

    scanf("%s", s+1);
    f[0] = 1;
    for(i=1; s[i]; i++) {
        s0[i] = s0[i - 1] + (s[i] == '0');
        s1[i] = s1[i - 1] + (s[i] == '1');
        for(j=i-2, k=i-1; j>=0; j-=2, k--) 
            if(!(s1[k] - s1[j]) and !(s0[i] - s0[k]))
                f[i] = (f[i] + f[j]) % MOD;
    }
    printf("%d\n", f[i - 1]);
    return 0;
}