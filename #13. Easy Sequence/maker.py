#!/usr/bin/python3

from cyaron import IO, randint
from random import random
from math import sqrt, log2

_N = [5, 100000, 3000, 3000, 100000]
_T = [5, 5, 5, 8, 10]

for T in range(5):
    for t in range(_T[T]):
        print('processing', T + 1, t + 1)
        data = IO(file_prefix='', data_id='%d-%d' % (T + 1, t + 1))

        n = randint(_N[T] * 8 // 10, _N[T])
        if t:
            n = n // 2 * 2

        s = ''
        x = n // 2 * 2
        if t & 1:
            step = int(sqrt(n))
        else:
            step = n - int(log2(n))
        step = max(step, 2)
        while x:
            r = min(step, x)
            l = randint(max(2, r // 2), r) // 2
            s += '0' * l + '1' * l
            x -= l * 2

        if T == 1:
            s = list(s)
            s[randint(0, n - 1)] = '?'
            s = ''.join(s)
        elif T == 2:
            s = '?' * n
        else:
            p = 0.5 * t / (_T[T] - 1) + 0.45
            start = int(randint(50000, 80000) / 100000 * t / (_T[T] - 1) * n)
            ss = '?' * start
            for ch in s[start:]:
                if random() <= p:
                    ss += '?'
                else:
                    ss += ch
            s = ss
        
        data.input_writeln(s)
        data.output_gen('./std')
        data.close()
