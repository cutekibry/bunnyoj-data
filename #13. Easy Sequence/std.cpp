#include <algorithm>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <string>
#include <vector>

using namespace std;

inline int read() {
    int res = 0, fh = 1;
    char ch = getchar();
    while ((ch > '9' || ch < '0') && ch != '-')
        ch = getchar();
    if (ch == '-')
        fh = -1, ch = getchar();
    while (ch >= '0' && ch <= '9')
        res = res * 10 + ch - '0', ch = getchar();
    return fh * res;
}

#define mod 1000000009
const int maxn = 500010;
int n, f[maxn], nxt[maxn], tail, sum = 0;
int to[maxn], nt[maxn], head[maxn], tot = 1;

inline void add(int x, int y) {
    if (x <= n) {
        to[++tot] = y;
        nt[tot] = head[x];
        head[x] = tot;
    }
}

inline void del(int x) {
    for (int i = head[x]; i; i = nt[i]) {
        if (to[i] >= tail)
            sum = (sum - f[to[i]] + mod) % mod;
        f[to[i]] = 0;
    }
}

char s[maxn];

int main() {
    scanf("%s", s + 1);
    n = strlen(s + 1);
    if (n & 1)
        return puts("0"), 0;
    nxt[n] = n + 1;
    for (int i = n - 1; i + 1; i--)
        if (s[i + 1] == '1')
            nxt[i] = i + 1;
        else
            nxt[i] = nxt[i + 1];
    for (int i = 0; i <= n; i += 2)
        add((nxt[i] << 1) - i, i);
    f[0] = sum = 1;
    for (int i = 2; i <= n; i += 2) {
        del(i);
        if (!(s[i] - '0'))
            sum = 0, tail = i;
        else if (s[i - 1] == '0')
            sum = f[i - 2], tail = i - 2;
        else {
            tail -= 2;
            if (tail >= 0)
                sum = (sum + f[tail]) % mod;
        }
        sum = (sum + (f[i] = sum)) % mod;
    }
    printf("%d\n", f[n]);
    return 0;
}