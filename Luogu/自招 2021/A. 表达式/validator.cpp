#include <testlib.h>

int main(int argc, char *argv[]) {
    registerValidation();

    ensuref(argc == 3, "argc n, ai must provided");

    int N = atoi(argv[1]);
    int A = atoi(argv[2]);

    int n = inf.readInt(1, N, "n");
    inf.readEoln();
    for (int i = 1; i <= n; i++) {
        inf.readInt(1, A, format("A[%d]", i));
        if (i < n)
            inf.readSpace();
    }
    inf.readEoln();
    inf.readEof();
}