#include <cstdio>

const int N = 2e5 + 10;

int n;
int a[N];

int main() {
    int i, j;
    long long ans = 0;

    scanf("%d", &n);
    for (i = 1; i <= n; i++)
        scanf("%d", &a[i]);
    for (i = 1; i <= n; i++)
        for (j = i + 1; j <= n; j++)
            ans += a[i] - a[j];
    printf("%lld\n", ans);
    return 0;
}
