#!/usr/bin/pypy3

from cyaron import IO, randint, ati
from math import sqrt

N = ati([1000] * 8 + [2e5] * 2)
V = ati([1000] * 9 + [1e9])

for t in range(10):
    print('# generating', t + 1)
    data  = IO(file_prefix='expr', data_id = t + 1)
    n = randint(N[t] - int(sqrt(N[t])), N[t])

    data.input_writeln(n)
    data.input_writeln([randint(1, V[t]) for i in range(n)])
    data.output_gen('./std')
    data.close()