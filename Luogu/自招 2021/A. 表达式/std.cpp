#include <cstdio>

const int N = 2e5 + 10;

int n;
int a[N];

int main() {
    int i;
    long long ans = 0;

    scanf("%d", &n);
    for (i = 1; i <= n; i++)
        scanf("%d", &a[i]);
    for (i = 1; i <= n; i++)
        ans += 1LL * ((n - i) - (i - 1)) * a[i];
    printf("%lld\n", ans);
    return 0;
}
