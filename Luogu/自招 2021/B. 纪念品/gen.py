#!/usr/bin/pypy3

from cyaron import IO, randint, ati
from math import sqrt

N = ati([100] * 6 + [3000] * 2 + [2e5] * 2)
M = ati([2e5] * 6 + [1e7] * 2 + [5e8] + [5e13])
V = ati([1e4] * 9 + [1e9])

for t in range(10):
    print('# generating', t + 1)
    data  = IO(file_prefix='souvenir', data_id = t + 1)
    n = randint(N[t] - int(sqrt(N[t])), N[t])
    m = randint(M[t] - int(sqrt(M[t])), M[t])

    data.input_writeln(n, m)
    data.input_writeln([randint(1, V[t]) for i in range(n)])
    data.output_gen('./std')
    data.close()