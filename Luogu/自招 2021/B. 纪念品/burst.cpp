#include <cstdio>
#include <algorithm>

const int N = 2e5 + 10;

int n;
long long m;
int a[N];

int main() {
    int i, j;
    int ans = 0;
    long long sum = 0;

    scanf("%d %lld", &n, &m);
    for (i = 1; i <= n; i++)
        scanf("%d", &a[i]);
    for (i = 1; i <= n; i++) {
        sum = 0;
        for (j = i; j <= n; j++) {
            sum += a[j];
            if (sum <= m)
                ans = std::max(ans, j - i + 1);
        }
    }
    printf("%d\n", ans);
    return 0;
}