#include <cstdio>
#include <algorithm>

const int N = 2e5 + 10;

int n;
long long m;
int a[N];

int main() {
    int i, j;
    int ans = 0;
    long long sum = 0;

    scanf("%d %lld", &n, &m);
    for (i = 1; i <= n; i++)
        scanf("%d", &a[i]);
    j = 0;
    for (i = 1; i <= n; i++) {
        j = std::max(i, j);
        while (j <= n and sum + a[j] <= m)
            sum += a[j++];
        ans = std::max(ans, j - i);
        if (i < j)
            sum -= a[i];
    }
    printf("%d\n", ans);
    return 0;
}