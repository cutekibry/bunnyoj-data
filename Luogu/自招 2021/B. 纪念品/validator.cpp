#include <testlib.h>

int main(int argc, char *argv[]) {
    registerValidation();

    ensuref(argc == 4, "argc n, m, a must provided");

    int N = atoi(argv[1]);
    long long M = atoll(argv[2]);
    int A = atoi(argv[3]);

    int n = inf.readInt(1, N, "n");
    inf.readSpace();
    inf.readLong(1, M, "m");
    inf.readEoln();
    for (int i = 1; i <= n; i++) {
        inf.readInt(1, A, format("A[%d]", i));
        if (i < n)
            inf.readSpace();
    }
    inf.readEoln();
    inf.readEof();
}