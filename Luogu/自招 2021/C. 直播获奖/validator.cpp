#include <testlib.h>
#include <cstring>

bool c[100005];

int main(int argc, char *argv[]) {
    registerValidation();

    ensuref(argc == 2, "argc n must provided");

    int N = atoi(argv[1]);

    int T = inf.readInt(1, 10, "T");
    inf.readEoln();
    while (T--) {
        int n = inf.readInt(1, N, "n");
        inf.readEoln();
        n *= 2;

        memset(c, 0, sizeof(c));
        for (int i = 1; i <= n; i++) {
            int x = inf.readInt(1, n, format("p[%d]", i));
            ensuref(!c[x], "Value %d appears twice", x);
            c[x] = true;
            if (i < n)
                inf.readSpace();
        }
        inf.readEoln();
    }
    inf.readEof();
}