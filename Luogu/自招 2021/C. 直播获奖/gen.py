#!/usr/bin/pypy3

from cyaron import IO, randint, ati
from math import sqrt

N = ati([6] * 5 + [500] * 2 + [5e4] * 3)

for t in range(10):
    print('# generating', t + 1)
    data = IO(file_prefix='stream', data_id=t + 1)

    T = randint(9, 10)
    data.input_writeln(T)
    while T:
        T -= 1
        if N[t] == 6:
            n = randint(1, N[t])
        else:
            n = randint(N[t] - int(sqrt(N[t])), N[t])

        if randint(0, 1):
            n = min(N[t], n >> 1 << 1 | 1)

        if n % 2 == 1:
            b = []
            pos = 1
            while pos != n:
                b.append(pos)
                b.append(pos + 1)
                b.append(n + pos + 1)
                b.append(n + pos + 2)
                pos += 2
            b.append(n)
            b.append(n + 1)

            m = [-1] * (2 * n + 1)
            for i, x in enumerate(b):
                m[x] = i

            p = []
            k = randint(0, int(1e9))
            for i in range(1, 2 * n + 1):
                p.append(b[(m[i] + (k if i % 2 == 1 else -k)) % (2 * n)])
        else:
            p = list(range(1, 2 * n + 1))
            k = randint(0, 25)
            for i in range(k):
                if i % 2 == 0:
                    for j in range(n):
                        p[2 * j], p[2 * j + 1] = p[2 * j + 1], p[2 * j]
                else:
                    for j in range(n):
                        p[j], p[j + n] = p[j + n], p[j]

        if T <= 1:
            i = randint(0, 2 * n - 1)
            j = randint(0, 2 * n - 1)
            while j == i:
                j = randint(0, 2 * n - 1)
            p[i], p[j] = p[j], p[i]

        data.input_writeln(n)
        data.input_writeln(p)
    data.output_gen('./std')
    data.close()
