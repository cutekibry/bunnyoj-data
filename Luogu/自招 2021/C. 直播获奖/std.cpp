// 原题：https://codeforces.com/problemset/problem/1510/K
// 原题数据 n <= 1000，此处加强了一点，不过做法类似，只是需要一点优化
// 题解可以在网上找到

#include <cstdio>
#include <algorithm>

const int N = 5e4 + 10;

int n;
int p[N << 1];

int solve0() {
    int i, j, k;

    if (p[1] == 1)
        k = 0;
    else if (p[2] == 1)
        k = 1;
    else if (p[n + 2] == 1)
        k = 2;
    else
        k = 3;
    for (i = 1; i <= k; i++) {
        if (i & 1)
            for (j = 1; j <= n; j++)
                std::swap(p[2 * j - 1], p[2 * j]);
        else
            for (j = 1; j <= n; j++)
                std::swap(p[j], p[j + n]);
    }
    for (i = 1; i <= 2 * n; i++)
        if (p[i] != i)
            return -1;
    return std::min(k, 4 - k);
}
int solve1() {
    static int d[N << 1], map[N << 1];
    int i, k, x;
    int dn = 0;

    i = 1;
    while (i < n) {
        d[dn++] = i;
        d[dn++] = i + 1;
        d[dn++] = i + 1 + n;
        d[dn++] = i + 2 + n;
        i += 2;
    }
    d[dn++] = n;
    d[dn++] = n + 1;

    for (i = 0; i < dn; i++)
        map[d[i]] = i;

    k = 0;
    while (p[d[k]] != 1)
        k++;
    for (i = 1; i <= 2 * n; i++) {
        x = map[i];
        if (i % 2 == 1)
            x += k;
        else
            x += 2 * n - k;
        x %= 2 * n;
        if (p[d[x]] != i)
            return -1;
    }
    return std::min(k, 2 * n - k);
}

int main() {
    int T;
    int i;

    scanf("%d", &T);
    while (T--) {
        scanf("%d", &n);
        for (i = 1; i <= 2 * n; i++)
            scanf("%d", &p[i]);
        if (n % 2 == 1)
            printf("%d\n", solve1());
        else
            printf("%d\n", solve0());
    }
    return 0;
}