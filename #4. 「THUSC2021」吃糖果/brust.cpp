// Author: Tsukimaru Oshawott
// In the name of
//
//      _/_/      _/_/_/        _/_/_/    _/_/_/_/    _/    _/    _/_/_/_/
//    _/    _/    _/    _/    _/          _/          _/    _/    _/
//    _/_/_/_/    _/_/_/_/    _/          _/_/_/_/    _/    _/    _/_/_/_/
//    _/    _/    _/  _/      _/          _/          _/    _/          _/
//    _/    _/    _/    _/      _/_/_/    _/_/_/_/    _/_/_/_/    _/_/_/_/
//

#include <algorithm>
#include <cassert>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>

#define For(i, l, r) for (i = int64(l); i <= int64(r); i++)
#define Fo(i, n) For(i, 1, n)
#define Rof(i, r, l) for (i = int64(r); i >= int64(l); i--)
#define Ro(i, n) Rof(i, n, 1)
#define clr(a) memset(a, 0, sizeof(a))
#define cpy(a, b) memcpy(a, b, sizeof(b))
#define fc(a, ch) memset(a, ch, sizeof(a))

typedef unsigned uint;
typedef unsigned long long uint64;
typedef long long int64;

#define T1 template <class A>
#define T2 template <class A, class B>
#define T3 template <class A, class B, class C>
#define T4 template <class A, class B, class C, class D>
inline void read(char &x) {
    do
        x = getchar();

    while (x <= ' ');
}
inline void read(char *s) {
    char ch;

    do
        ch = getchar();

    while (ch <= ' ');

    while (ch > ' ') {
        (*s) = ch;
        s++;
        ch = getchar();
    }

    (*s) = 0;
}
inline void read(std::string &s) {
    char ch;

    do
        ch = getchar();

    while (ch <= ' ');

    while (ch > ' ') {
        s.push_back(ch);
        ch = getchar();
    }
}
T1 inline void readint(A &x) {
    bool neg = false;
    char ch;

    do
        ch = getchar();

    while (ch <= ' ');

    if (ch == '-') {
        neg = true;
        ch = getchar();
    }

    x = 0;

    while (ch > ' ') {
        x = x * 10 + (ch & 15);
        ch = getchar();
    }

    if (neg)
        x = -x;
}
inline void read(int &x) { readint(x); }
inline void read(uint &x) { readint(x); }
inline void read(int64 &x) { readint(x); }
inline void read(uint64 &x) { readint(x); }

T2 inline void read(A &a, B &b) {
    read(a);
    read(b);
}
T3 inline void read(A &a, B &b, C &c) {
    read(a);
    read(b);
    read(c);
}
T4 inline void read(A &a, B &b, C &c, D &d) {
    read(a);
    read(b);
    read(c);
    read(d);
}
inline void writeln() { putchar('\n'); }
T1 inline void writeint(A x) {
    static char buf[20];
    int top = 0;

    if (!x) {
        putchar('0');
        return;
    }

    if (x < 0) {
        putchar('-');
        x = -x;
    }

    while (x) {
        buf[++top] = (x % 10) | 48;
        x /= 10;
    }

    while (top)
        putchar(buf[top--]);
}
inline void write(int x) { writeint(x); }
inline void write(uint x) { writeint(x); }
inline void write(int64 x) { writeint(x); }
inline void write(uint64 x) { writeint(x); }
inline void write(char ch) { putchar(ch); }
inline void write(char *s) {
    while (*s) {
        putchar(*s);
        s++;
    }
}
inline void write(const char *s) { printf(s); }
T1 inline void write_(A x) {
    write(x);
    putchar(' ');
}
T1 inline void writeln(A x) {
    write(x);
    putchar('\n');
}
T2 inline void write(A a, B b) {
    write_(a);
    write(b);
}
T2 inline void writeln(A a, B b) {
    write_(a);
    writeln(b);
}
T3 inline void writeln(A a, B b, C c) {
    write_(a);
    write_(b);
    writeln(c);
}
T4 inline void writeln(A a, B b, C c, D d) {
    write_(a);
    write_(b);
    write_(c);
    writeln(d);
}
#undef T1
#undef T2
#undef T3
#undef T4

const int N = 1e5 + 10;

int n, m;
int pre[N << 1], to[N << 1], head[N], wcnt;
int deg[N];
int a[N];
int ans;

int P;
int seg[N << 2 | 5];

inline void addedge(int u, int v) {
    wcnt++;
    pre[wcnt] = head[u];
    head[u] = wcnt;
    to[wcnt] = v;
    deg[u]++;
}
inline void add2edge(int u, int v) {
    addedge(u, v);
    addedge(v, u);
}
void discrete() {
    static int num[N];
    int i;

    cpy(num, a);
    std::sort(num + 1, num + 1 + n);
    m = std::unique(num + 1, num + 1 + n) - num - 1;
    Fo(i, n) a[i] = std::lower_bound(num + 1, num + 1 + m, a[i]) - num;
}
inline void fix(int p, int x) {
    for (seg[p += P] = x, p >>= 1; p; p >>= 1)
        seg[p] = std::max(seg[p << 1], seg[p << 1 | 1]);
}
inline void upd(int p, int x) {
    if (seg[p + P] < x)
        fix(p, x);
}
inline int query(int l, int r) {
    int res = 0;
    for (l += P - 1, r += P + 1; l ^ r ^ 1; l >>= 1, r >>= 1) {
        if (~l & 1)
            res = std::max(res, seg[l ^ 1]);
        if (r & 1)
            res = std::max(res, seg[r ^ 1]);
    }
    return res;
}

namespace s4 {
    bool check() { return m <= 2; }
    void main() { writeln(m); }
}; // namespace s4

namespace s5 {
    bool check() {
        int i;
        Fo(i, n) if (deg[i] > 1) break;
        For(i, i + 1, n) if (deg[i] > 1) break;
        return i > n;
    }
    void main() {
        int p, minv = 1e9, maxv = -1e9, i;
        Fo(p, n) if (deg[p] > 1) break;
        Fo(i, n) if (i ^ p) {
            minv = std::min(minv, a[i]);
            maxv = std::max(maxv, a[i]);
        }
        writeln(2 + (minv < a[p] and a[p] < maxv));
    }
}; // namespace s5
namespace s6 {
    int f[N], b[N];

    bool check() {
        int i;
        Fo(i, n) if (deg[i] > 2) return false;
        return true;
    }
    void main() {
        int i, u, preu, t;
        int ans;

        Fo(u, n) if (deg[u] == 1) break;
        preu = 0;
        Fo(i, n) {
            b[i] = a[u];
            t = u;
            u = preu ^ to[head[u]] ^ to[pre[head[u]]];
            preu = t;
        }

        Fo(i, n) {
            f[i] = query(1, b[i] - 1) + 1;
            upd(b[i], f[i]);
        }
        ans = seg[1];
        clr(seg);
        Ro(i, n) {
            f[i] = query(1, b[i] - 1) + 1;
            upd(b[i], f[i]);
        }
        ans = std::max(ans, seg[1]);
        writeln(ans);
    }
} // namespace s6
namespace sany {
    int ans;
    void dfs(int u, int fa) {
        int i, t;

        t = seg[a[u] + P];
        upd(a[u], query(1, a[u] - 1) + 1);
        ans = std::max(ans, seg[1]);
        for (i = head[u]; i; i = pre[i])
            if (to[i] ^ fa)
                dfs(to[i], u);
        fix(a[u], t);
    }
    void main() {
        int s;
        Fo(s, n) dfs(s, 0);
        writeln(ans);
    }
} // namespace sany

int main() {
    int i;
    int u, v;

    seg[0] = -1e9;

    read(n);
    Fo(i, n) read(a[i]);
    Fo(i, n - 1) {
        read(u, v);
        add2edge(u, v);
    }
    discrete();

    P = 1;
    while (P <= m)
        P <<= 1;

    if (s4::check())
        s4::main();
    else if (s5::check())
        s5::main();
    else if (s6::check())
        s6::main();
    else
        sany::main();
    return 0;
}