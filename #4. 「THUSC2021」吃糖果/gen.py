#!/usr/bin/pypy3

from cyaron import IO, randint
from random import shuffle
from math import sqrt
from sys import argv
from os import system


V = int(1e9)


def gen_random3(N: int, L: int, R: int):
    n = randint(max(1, N - int(sqrt(N))), N)
    a = [randint(L, R) for i in range(n)]
    e = [(randint(1, i - 1), i) for i in range(2, n + 1)]
    return (n, a, e)


def gen_random(N: int = 100000, R: int = V):
    return gen_random3(N, 1, R)


def gen_constructed(N: int, V: int, t: int):
    n = randint(N - int(sqrt(N)), N)
    B = sqrt(n)

    fa = [-1] + [0] * n
    mode = [-1] + [False] * n
    a = [-1] + [0] * n

    a[1] = max(1, V // 2 + randint(-int(sqrt(V)), int(sqrt(V))))

    if t == 0:
        p = int(sqrt(B))
    elif t == 1:
        p = B
    else:
        p = n // 2

    lock = 0
    for i in range(2, n + 1):
        if i > 2 and randint(1, n) <= B:
            lock = randint(1, int(sqrt(B)))

        if lock > 0:
            lock -= 1
            fa[i] = fa[i - 1]
        else:
            fa[i] = randint(max(i - 3, 1), i - 1)

        if randint(1, n) <= p:
            mode[i] = not mode[fa[i]]

        if mode[i]:
            a[i] = randint(1, a[fa[i]])
        else:
            a[i] = randint(a[fa[i]], V)

    a = a[1:]
    e = [(i, fa[i]) for i in range(2, n + 1)]
    return (n, a, e)


def gen3(N:int=5000):
    n = randint(N - int(sqrt(N)), N)
    B = sqrt(n)

    fa = [-1] + [0] * n
    a = [-1] + [0] * n

    a[1] = randint(3, 4)

    lock = 0
    for i in range(2, n + 1):
        fa[i] = randint(1, i - 1)

        if i * 2 <= n:
            a[i] = randint(3, 4)
        else:
            a[i] = a[fa[i]] if a[fa[i]] <= 2 else randint(1, 2)

    a = a[1:]
    e = [(i, fa[i]) for i in range(2, n + 1)]
    return (n, a, e)


def gen4(t: int):  # t = 0..1
    return gen_random3(100000, 2 - t, 2)


def gen5(t: int, N: int=100000):  # t = 0..3
    n = randint(N-int(sqrt(N)), N)
    val = randint(2, V - 1)
    if t == 0:
        a = [val] * n
    elif t == 1:
        a = [randint(1, V) for i in range(n)]
    elif t == 2:
        a = [randint(1, val - 1) for i in range(n)]
        a[randint(2, n) - 1] = val
    else:
        a = [randint(val + 1, V) for i in range(n)]
        a[randint(2, n) - 1] = val

    a[1 - 1] = val

    e = [(1, i) for i in range(2, n + 1)]

    return (n, a, e)


def gen6(t: int, N: int = 100000):  # t = times of swap
    n = randint(N-int(sqrt(N)), N)
    a = [randint(1, V) for i in range(n)]
    for i in range(min(t, n)):
        k = randint(0, n - 1)
        a[i], a[k] = a[k], a[i]
    e = [(i, i + 1) for i in range(1, n)]
    return (n, a, e)


data_cnt = 0


def write(res: tuple):
    global data_cnt

    data_cnt += 1
    data = IO(file_prefix='candy', data_id=data_cnt)

    print('# generating candy%d.in' % data_cnt)

    n, a0, e = res

    permutation = list(range(1, n + 1))
    shuffle(permutation)
    permutation = [-1] + permutation

    a = [0] * n
    for i, x in enumerate(a0):
        a[permutation[i + 1] - 1] = x

    data.input_writeln(n)
    data.input_writeln(a)

    shuffle(e)
    for u, v in e:
        if randint(0, 1):
            u, v = v, u
        data.input_writeln(permutation[u], permutation[v])

    print('# generating candy%d.out' % data_cnt)
    data.output_gen('./std')
    data.close()

    print('# validate candy%d.in' % data_cnt)
    if system('./val < candy%d.in' % data_cnt) != 0:
        print('# !!! validate failed !!!')
        exit(1)



def gen_pretests():
    write(gen_random(10, 10))
    write(gen_random(10, 10))
    write(gen_random(1000))
    write(gen_random(1000))
    write(gen_random(5000, 4))
    write(gen_random(5000, 4))
    write(gen_random(100000, 2))
    write(gen_random(100000, 2))
    write(gen5(1))
    write(gen5(1))
    write(gen6(100000))
    write(gen6(100000))
    write(gen_random())
    write(gen_random())


def gen_final_tests():
    global data_cnt

    if data_cnt != 14:
        data_cnt = 14

    print('# sub 1')
    write(gen_random(10, 10))
    write(gen_random(10, 10))
    write(gen_random(10, 10))

    print('# sub 2')
    write(gen_random(1000))
    write(gen_constructed(1000, V, 0))
    write(gen_constructed(1000, V, 1))
    write(gen_constructed(1000, V, 2))

    print('# sub 3')
    write(gen_random(5000, 4))
    write(gen3(5000))
    write(gen_constructed(5000, 3, 1))
    write(gen_constructed(5000, 4, 2))

    print('# sub 4')
    write(gen4(0))
    write(gen4(1))

    print('# sub 5')
    write(gen_random(1))
    write(gen5(1, 5000))
    write(gen5(0))
    write(gen5(1))
    write(gen5(2))
    write(gen5(3))

    print('# sub 6')
    write(gen_random(1))
    write(gen6(10, 5000))
    write(gen6(10))
    write(gen6(100))
    write(gen6(1000))
    write(gen6(10000))

    print('# sub 7')
    write(gen_constructed(5000, V, 0))
    write(gen_random())
    write(gen_random())
    write(gen_constructed(100000, V, 0))
    write(gen_constructed(100000, V, 1))
    write(gen_constructed(100000, V, 2))


if __name__ == '__main__':
    if len(argv) == 1 or argv[1] not in ['pretest', 'final']:
        print('unrecognized argument')
    else:
        system('g++ std.cpp -o std -O3 -std=c++11')
        system('g++ val.cpp -o val -O3 -std=c++11')
        if argv[1] == 'pretest':
            gen_pretests()
        else:
            gen_final_tests()
