#include "testlib.h"

const int N = 1e5 + 10;

int fa[N];
int gf(int x) { return (x ^ fa[x]) ? (fa[x] = gf(fa[x])) : x; }

int main(int argc, char *argv[]) {
    registerValidation();

    int n, i, u, v;

    n = inf.readInt(1, 100000, "n");
    inf.readEoln();

    for (i = 1; i <= n; i++)
        fa[i] = i;

    for (i = 1; i <= n; i++) {
        inf.readInt(1, 1000000000, format("a[%d]", i));
        if (i < n)
            inf.readSpace();
    }
    inf.readEoln();

    for (i = 1; i < n; i++) {
        u = inf.readInt(1, n, format("u[%d]", i));
        inf.readSpace();
        v = inf.readInt(1, n, format("v[%d]", i));
        inf.readEoln();
        ensuref(gf(u) != gf(v), "gf(%d) == gf(%d) at %d-th edge", u, v, i);
        fa[gf(u)] = gf(v);
    }

    inf.readEof();
}