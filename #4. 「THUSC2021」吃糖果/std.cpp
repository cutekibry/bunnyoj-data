// Author: Tsukimaru Oshawott
// In the name of
//
//      _/_/      _/_/_/        _/_/_/    _/_/_/_/    _/    _/    _/_/_/_/
//    _/    _/    _/    _/    _/          _/          _/    _/    _/
//    _/_/_/_/    _/_/_/_/    _/          _/_/_/_/    _/    _/    _/_/_/_/
//    _/    _/    _/  _/      _/          _/          _/    _/          _/
//    _/    _/    _/    _/      _/_/_/    _/_/_/_/    _/_/_/_/    _/_/_/_/
//

#include <algorithm>
#include <cassert>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>

#define For(i, l, r) for (i = int64(l); i <= int64(r); i++)
#define Fo(i, n) For(i, 1, n)
#define Rof(i, r, l) for (i = int64(r); i >= int64(l); i--)
#define Ro(i, n) Rof(i, n, 1)
#define clr(a) memset(a, 0, sizeof(a))
#define cpy(a, b) memcpy(a, b, sizeof(b))
#define fc(a, ch) memset(a, ch, sizeof(a))

typedef unsigned uint;
typedef unsigned long long uint64;
typedef long long int64;

#define T1 template <class A>
#define T2 template <class A, class B>
#define T3 template <class A, class B, class C>
#define T4 template <class A, class B, class C, class D>
inline void read(char &x) {
    do
        x = getchar();

    while (x <= ' ');
}
inline void read(char *s) {
    char ch;

    do
        ch = getchar();

    while (ch <= ' ');

    while (ch > ' ') {
        (*s) = ch;
        s++;
        ch = getchar();
    }

    (*s) = 0;
}
inline void read(std::string &s) {
    char ch;

    do
        ch = getchar();

    while (ch <= ' ');

    while (ch > ' ') {
        s.push_back(ch);
        ch = getchar();
    }
}
T1 inline void readint(A &x) {
    bool neg = false;
    char ch;

    do
        ch = getchar();

    while (ch <= ' ');

    if (ch == '-') {
        neg = true;
        ch = getchar();
    }

    x = 0;

    while (ch > ' ') {
        x = x * 10 + (ch & 15);
        ch = getchar();
    }

    if (neg)
        x = -x;
}
inline void read(int &x) { readint(x); }
inline void read(uint &x) { readint(x); }
inline void read(int64 &x) { readint(x); }
inline void read(uint64 &x) { readint(x); }

T2 inline void read(A &a, B &b) {
    read(a);
    read(b);
}
T3 inline void read(A &a, B &b, C &c) {
    read(a);
    read(b);
    read(c);
}
T4 inline void read(A &a, B &b, C &c, D &d) {
    read(a);
    read(b);
    read(c);
    read(d);
}
inline void writeln() { putchar('\n'); }
T1 inline void writeint(A x) {
    static char buf[20];
    int top = 0;

    if (!x) {
        putchar('0');
        return;
    }

    if (x < 0) {
        putchar('-');
        x = -x;
    }

    while (x) {
        buf[++top] = (x % 10) | 48;
        x /= 10;
    }

    while (top)
        putchar(buf[top--]);
}
inline void write(int x) { writeint(x); }
inline void write(uint x) { writeint(x); }
inline void write(int64 x) { writeint(x); }
inline void write(uint64 x) { writeint(x); }
inline void write(char ch) { putchar(ch); }
inline void write(char *s) {
    while (*s) {
        putchar(*s);
        s++;
    }
}
inline void write(const char *s) { printf(s); }
T1 inline void write_(A x) {
    write(x);
    putchar(' ');
}
T1 inline void writeln(A x) {
    write(x);
    putchar('\n');
}
T2 inline void write(A a, B b) {
    write_(a);
    write(b);
}
T2 inline void writeln(A a, B b) {
    write_(a);
    writeln(b);
}
T3 inline void writeln(A a, B b, C c) {
    write_(a);
    write_(b);
    writeln(c);
}
T4 inline void writeln(A a, B b, C c, D d) {
    write_(a);
    write_(b);
    write_(c);
    writeln(d);
}
#undef T1
#undef T2
#undef T3
#undef T4

const int N = 2e5 + 10;

int n;
int pre[N << 1], to[N << 1], head[N], wcnt;
int a[N];
int seg[N * 40], lson[N * 40], rson[N * 40], nodecnt;
int frt[N], grt[N];
int ans;

#define lp lson[p]
#define rp rson[p]
#define mid ((l + r) >> 1)
#define lc lp, l, mid
#define rc rp, mid + 1, r
void merge(int &x, int y) {
    if (!x or !y)
        x |= y;
    else {
        seg[x] = std::max(seg[x], seg[y]);
        merge(lson[x], lson[y]);
        merge(rson[x], rson[y]);
    }
}
void updans(int x, int y, int xmax, int ymax) {
    ans = std::max(ans, seg[x] + ymax);
    ans = std::max(ans, seg[y] + xmax);
    if (x and y) {
        updans(lson[x], lson[y], xmax, std::max(seg[rson[y]], ymax));
        updans(rson[x], rson[y], std::max(xmax, seg[lson[x]]), ymax);
    }
}
void ins(int &p, int l, int r, int x, int y) {
    if (!p)
        p = ++nodecnt;
    seg[p] = std::max(seg[p], y);
    if (l == r)
        return;
    else if (x <= mid)
        ins(lson[p], l, mid, x, y);
    else
        ins(rson[p], mid + 1, r, x, y);
}
int query(int p, int l, int r, int a, int b) {
    if (!p or (a <= l and r <= b))
        return seg[p];
    else if (a <= mid and b > mid)
        return std::max(query(lc, a, b), query(rc, a, b));
    else if (a <= mid)
        return query(lc, a, b);
    else
        return query(rc, a, b);
}
inline int query(int p, int a, int b) { return (a > b) ? -1e9 : query(p, 1, n, a, b); }
#undef mid

inline void addedge(int u, int v) {
    wcnt++;
    pre[wcnt] = head[u];
    head[u] = wcnt;
    to[wcnt] = v;
}
inline void add2edge(int u, int v) {
    addedge(u, v);
    addedge(v, u);
}
void dfs(int u, int fa) {
    ins(frt[u], 1, n, a[u], 1);
    ins(grt[u], 1, n, a[u], 1);
    for (int i = head[u]; i; i = pre[i])
        if (to[i] ^ fa) {
            dfs(to[i], u);
            updans(frt[u], grt[to[i]], 0, 0);
            updans(frt[to[i]], grt[u], 0, 0);
            ins(frt[u], 1, n, a[u], query(frt[to[i]], 1, a[u] - 1) + 1);
            ins(grt[u], 1, n, a[u], query(grt[to[i]], a[u] + 1, n) + 1);
            merge(frt[u], frt[to[i]]);
            merge(grt[u], grt[to[i]]);
        }
}
void discrete() {
    static int num[N];
    int i;

    cpy(num, a);
    std::sort(num + 1, num + 1 + n);
    Fo(i, n) a[i] = std::lower_bound(num + 1, num + 1 + n, a[i]) - num;
}

int main() {
    int i;
    int u, v;

    seg[0] = -1e9;

    read(n);
    Fo(i, n) read(a[i]);
    Fo(i, n - 1) {
        read(u, v);
        add2edge(u, v);
    }
    discrete();
    dfs(1, 0);
    ans = std::max(ans, std::max(seg[frt[1]], seg[grt[1]]));
    writeln(ans);
    return 0;
}