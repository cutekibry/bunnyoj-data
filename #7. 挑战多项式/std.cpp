// std by Snakes (https://loj.ac/s/226437)

#include <algorithm>
#include <cmath>
#include <cstdio>
#include <cstring>

using namespace std;

#define REP(v, a, b) for ((v) = (a); (v) <= (b); (v)++)
#define REPD(v, a, b) for ((v) = (a); (v) >= (b); (v)--)
#define REPS(v, a, b, s) for ((v) = (a); (v) <= (b); (v) += (s))
#define REPDS(v, a, b, s) for ((v) = (a); (v) >= (b); (v) -= (s))
#define REPL(v, a, b) for ((v) = (a); (v) <= (b); (v) <<= 1)
#define REPR(v, a, b) for ((v) = (a); (v) >= (b); (v) >>= 1)
#define REPIF(v, a, b) for ((v) = (a); (b); (v)++)
#define REPIFD(v, a, b) for ((v) = (a); (b); (v)--)
#define REPIFS(v, a, b, s) for ((v) = (a); (b); (v) += (s))
#define REPIFDS(v, a, b, s) for ((v) = (a); (b); (v) -= (s))
#define REPIFL(v, a, b) for ((v) = (a); (b); (v) <<= 1)
#define REPIFR(v, a, b) for ((v) = (a); (b); (v) >>= 1)

const int maxn = 6e5;

const long long mod = 998244353;
const long long inv2 = 499122177;
const long long mrt = 31596;
const int g = 3;

typedef int coefficients[maxn];

const int hsz = 311100;
const long long hmod = 311021;

int val[hsz], mln[hsz];
bool used[hsz];

inline void __usr_ins(int v, int p) {
    static int h, j;
    h = v % hmod;
    j = 1;
    while (used[h] && val[h] != v) {
        h = ((long long)h + (long long)j * j) % mod;
        j++;
    }
    used[h] = 1;
    val[h] = v;
    mln[h] = p;
}

inline int __usr_qry(int v) {
    static int h, j;
    h = v % hmod;
    j = 1;
    while (used[h] && val[h] != v) {
        h = ((long long)h + (long long)j * j) % mod;
        j++;
    }
    if (used[h])
        return mln[h];
    return -1;
}

inline int __usr_clz(int v) { return v < 2 ? v + 1 : 1 << (33 - __builtin_clz(v - 1)); }

inline int __usr_fpw(int p, int e) {
    if (e == 0)
        return 1;
    long long ret = __usr_fpw(p, e >> 1);
    ret = ret * ret % mod;
    if (e & 1)
        ret = ret * p % mod;
    return ret;
}

inline int __usr_inv(int p) { return __usr_fpw(p, mod - 2); }

inline int __usr_sqrt(int p) {
    static int i, j, res;
    if (p == 0)
        return 0;
    j = 1;
    REP(i, 0, mrt - 1) {
        __usr_ins(j, i);
        j = (long long)j * g % mod;
    }
    res = 0;
    j = __usr_inv(j);
    REP(i, 0, mrt) {
        if (__usr_qry(p) != -1) {
            res = (res + __usr_qry(p)) % (mod - 1);
            if (res & 1)
                return -1;
            res = __usr_fpw(g, res / 2);
            if (mod - res < res)
                res = mod - res;
            return res;
        }
        p = (long long)p * j % mod;
        res = (res + mrt) % (mod - 1);
    }
    return -1;
}

inline void read(int &x) {
    static char c;
    static bool neg;
    x = 0;
    c = getchar();
    neg = 0;
    while (c != '-' && (c < '0' || c > '9'))
        c = getchar();
    if (c == '-') {
        neg = 1;
        while (c < '0' || c > '9')
            c = getchar();
    }
    while (c >= '0' && c <= '9') {
        x = x * 10 + c - '0';
        c = getchar();
    }
    if (neg)
        x = -x;
}

inline void write(int x, int endl = 0) {
    static char s[25];
    static int i, j;
    i = 0;
    s[i] = 0 + '0';
    while (x) {
        s[i] = x % 10 + '0';
        i++;
        x /= 10;
    }
    if (i == 0)
        i++;
    s[i] = 0;
    if (endl) {
        for (j = 0; j + j + 1 < i; j++)
            s[j] ^= s[i - 1 - j] ^= s[j] ^= s[i - 1 - j];
        puts(s);
    }
    else {
        while (i) {
            i--;
            putchar(s[i]);
        }
        putchar(' ');
    }
}

struct polynomial {
    coefficients val;
    int len;
    polynomial(int v) {
        memset(val, 0, sizeof(coefficients));
        val[0] = v;
        len = 1;
    }
    polynomial(int v[]) {
        REPIFD(len, maxn - 1, len > 0 && v[len - 1] == 0)
        val[len] = 0;
        val[len] = 0;
        static int i;
        REP(i, 0, len - 1)
        val[i] = v[i];
    }
    polynomial() {
        memset(val, 0, sizeof(coefficients));
        len = 0;
    }
    void init(int v) {
        memset(val, 0, sizeof(coefficients));
        val[0] = v;
        len = 1;
    }
    void init(int v[]) {
        REPIFD(len, maxn - 1, len > 0 && v[len - 1] == 0)
        val[len] = 0;
        val[len] = 0;
        static int i;
        REP(i, 0, len - 1)
        val[i] = v[i];
    }
    void init() {
        memset(val, 0, sizeof(coefficients));
        len = 0;
    }
    void reinit(int v = -1) {
        if (v >= 0) {
            len = v;
            return;
        }
        REPIFD(len, maxn - 1, len > 0 && val[len - 1] == 0);
    }
    void reverse() {
        static int i;
        REP(i, 0, (len >> 1) - 1)
        val[i] ^= val[len - 1 - i] ^= val[i] ^= val[len - 1 - i];
    }
    void reserve(int v = 0) {
        static int i;
        REP(i, v, len - 1)
        val[i] = 0;
        len = v;
    }
    void output(int rev = 0, int fastout = 0) {
        static int i;
        if (rev)
            REP(i, 0, len - 1) {
                if (!fastout)
                    printf(i < len - 1 ? "%d " : "%d\n", val[i]);
                else if (i < len - 1)
                    write(val[i]);
                else
                    write(val[i], 1);
            }
        else
            REPD(i, len - 1, 0) {
                if (!fastout)
                    printf(i ? "%d " : "%d\n", val[i]);
                else if (i > 0)
                    write(val[i]);
                else
                    write(val[i], 1);
            }
    }
    void output_len(int v, int rev = 0, int fastout = 0) {
        static int i;
        if (rev)
            REP(i, 0, v - 1) {
                if (!fastout)
                    printf(i < v - 1 ? "%d " : "%d\n", val[i]);
                else if (i < v - 1)
                    write(val[i]);
                else
                    write(val[i], 1);
            }
        else
            REPD(i, v - 1, 0) {
                if (!fastout)
                    printf(i ? "%d " : "%d\n", val[i]);
                else if (i > 0)
                    write(val[i]);
                else
                    write(val[i], 1);
            }
    }
};

namespace NTT {
int dft_len, dft_len_inv;
coefficients V, X, Y;
coefficients B, B0;
void dft(int A[], int p) {
    static int i, j, k;
    static long long x, y;
    j = 0;
    REP(i, 0, dft_len - 1) {
        if (i > j)
            A[i] ^= A[j] ^= A[i] ^= A[j];
        REPIFR(k, dft_len >> 1, (j ^= k) < k);
    }
    REP(i, 0, dft_len - 1)
    V[i] = A[i];
    REPL(i, 2, dft_len) {
        int gn = __usr_fpw(g, (mod - 1) / i);
        REPS(j, 0, dft_len - 1, i) {
            int G = 1;
            REP(k, j, j + (i >> 1) - 1) {
                x = V[k];
                y = (long long)V[k + (i >> 1)] * G % mod;
                V[k] = (x + y) % mod;
                V[k + (i >> 1)] = (x - y + mod) % mod;
                G = (long long)G * gn % mod;
            }
        }
    }
    if (p == -1) {
        REP(i, 1, (dft_len >> 1) - 1)
        V[i] ^= V[dft_len - i] ^= V[i] ^= V[dft_len - i];
        dft_len_inv = __usr_inv(dft_len);
        REP(i, 0, dft_len - 1)
        V[i] = (long long)V[i] * dft_len_inv % mod;
    }
    REP(i, 0, dft_len - 1)
    A[i] = V[i];
}
void inv(int A[], int len, int A0[] = X) {
    static int j, p, cur[30];
    p = 0;
    while (len > 1) {
        cur[p] = len;
        ;
        p++;
        len = (len + 1) >> 1;
    }
    cur[p] = len;
    REPD(j, p, 0) {
        if (cur[j] <= 0)
            return;
        if (cur[j] == 1) {
            memset(A0, 0, sizeof(coefficients));
            A0[0] = __usr_inv(A[0]);
            if (j == 0)
                memcpy(A, A0, sizeof(coefficients));
            continue;
        }
        dft_len = __usr_clz(cur[j] + 1);
        dft_len_inv = __usr_inv(dft_len);
        static int i;
        REP(i, 0, cur[j] - 1)
        Y[i] = A[i];
        REP(i, cur[j], dft_len - 1)
        Y[i] = 0;
        dft(Y, 1);
        dft(A0, 1);
        REP(i, 0, dft_len - 1)
        A0[i] = (long long)A0[i] * (mod + 2 - (long long)Y[i] * A0[i] % mod) % mod;
        dft(A0, -1);
        REP(i, cur[j], dft_len - 1)
        A0[i] = 0;
        if (j == 0)
            memcpy(A, A0, sizeof(coefficients));
    }
}
void root(int A[], int len, int A0[] = X) {
    static int j, p, cur[30];
    p = 0;
    while (len > 1) {
        cur[p] = len;
        p++;
        len = (len + 1) >> 1;
    }
    cur[p] = len;
    REPD(j, p, 0) {
        if (cur[j] <= 0)
            continue;
        if (cur[j] == 1) {
            memset(A0, 0, sizeof(coefficients));
            memset(B, 0, sizeof(coefficients));
            A0[0] = __usr_sqrt(A[0]);
            B[0] = __usr_inv(A0[0]);
            if (j == 0)
                memcpy(A, A0, sizeof(coefficients));
            continue;
        }
        static int i;
        dft_len = __usr_clz(cur[j] + 1);
        dft_len_inv = __usr_inv(dft_len);
        dft(B, 1);
        REP(i, 0, cur[j] - 1)
        Y[i] = A[i];
        REP(i, cur[j], dft_len - 1)
        Y[i] = 0;
        dft(Y, 1);
        dft(A0, 1);
        REP(i, 0, dft_len - 1)
        B0[i] = (long long)B[i] * (mod + 2 - (long long)A0[i] * B[i] % mod) % mod;
        dft(B0, -1);
        REP(i, cur[j], dft_len - 1)
        B0[i] = 0;
        dft(B0, 1);
        REP(i, 0, dft_len - 1)
        A0[i] = (inv2 * A0[i] + inv2 * Y[i] % mod * B0[i]) % mod;
        dft(A0, -1);
        REP(i, cur[j], dft_len - 1)
        A0[i] = 0;
        REP(i, 0, cur[j] - 1)
        Y[i] = A0[i];
        REP(i, cur[j], dft_len - 1)
        Y[i] = 0;
        dft(Y, 1);
        REP(i, 0, dft_len - 1)
        B[i] = (long long)B[i] * (mod + 2 - (long long)Y[i] * B[i] % mod) % mod;
        dft(B, -1);
        REP(i, cur[j], dft_len - 1)
        B[i] = 0;
        if (j == 0)
            memcpy(A, A0, sizeof(coefficients));
    }
}
} // namespace NTT

polynomial operator+(polynomial &x, polynomial &y) {
    static int i;
    int rep_len = max(x.len, y.len);
    static coefficients res;
    memset(res, 0, sizeof(coefficients));
    REP(i, 0, rep_len - 1)
    res[i] = (x.val[i] + y.val[i]) % mod;
    return polynomial(res);
}

polynomial operator-(polynomial &x, polynomial &y) {
    static int i;
    int rep_len = max(x.len, y.len);
    static coefficients res;
    memset(res, 0, sizeof(coefficients));
    REP(i, 0, rep_len - 1)
    res[i] = (mod + (x.val[i] - y.val[i]) % mod) % mod;
    return polynomial(res);
}

polynomial operator*(polynomial &x, polynomial &y) {
    static coefficients res, a, b;
    memset(res, 0, sizeof(coefficients));
    memset(a, 0, sizeof(coefficients));
    memset(b, 0, sizeof(coefficients));
    NTT::dft_len = max(__usr_clz(x.len), __usr_clz(y.len));
    static int i;
    REP(i, 0, x.len - 1)
    a[i] = x.val[i];
    REP(i, 0, y.len - 1)
    b[i] = y.val[i];
    NTT::dft(a, 1);
    NTT::dft(b, 1);
    REP(i, 0, NTT::dft_len - 1)
    res[i] = (long long)a[i] * b[i] % mod;
    NTT::dft(res, -1);
    return polynomial(res);
}

polynomial operator~(polynomial &x) {
    static polynomial res;
    res = x;
    NTT::inv(res.val, res.len);
    res.reinit();
    return res;
}

polynomial operator|(polynomial &x, int y) {
    static polynomial res;
    res = x;
    NTT::inv(res.val, y);
    res.reinit();
    return res;
}

polynomial operator&(polynomial &x, int y) {
    static polynomial res;
    static int i;
    res = x;
    if (y == -1) {
        REP(i, 0, res.len - 1)
        res.val[i] = ((long long)res.val[i + 1] * (i + 1)) % mod;
        res.val[res.len] = 0;
        res.reinit();
        return res;
    }
    if (y == 1) {
        REPD(i, res.len, 0)
        res.val[i + 1] = ((long long)res.val[i] * __usr_inv(i + 1)) % mod;
        res.val[0] = 0;
        res.reinit();
        return res;
    }
    return res;
}

polynomial operator!(polynomial &x) {
    static polynomial res;
    res = x;
    NTT::root(res.val, res.len);
    res.reinit();
    return res;
}

polynomial operator/(polynomial &x, polynomial &y) {
    static polynomial res;
    x.reverse();
    y.reverse();
    res = y | (x.len - y.len + 1);
    res = x * res;
    res.reserve(x.len - y.len + 1);
    res.reverse();
    x.reverse();
    y.reverse();
    return res;
}

polynomial operator%(polynomial &x, polynomial &y) {
    static polynomial res;
    res = x / y;
    res = res * y;
    res = x - res;
    return res;
}

polynomial operator&&(polynomial &x, int y) {
    if (y == -1) {
        static polynomial res, inv;
        res = x & -1;
        inv = ~x;
        res = res * inv;
        res = res & 1;
        return res;
    }
    if (y == 1) {
        static polynomial res, inv, lninv;
        static int j, p, k, len, cur[30];
        p = 0;
        len = x.len << 1;
        while (len > 1) {
            cur[p] = len;
            p++;
            len = (len + 1) >> 1;
        }
        res.init(1);
        REPD(j, p - 1, 0) {
            inv = res & -1;
            lninv = ~res;
            inv = inv * lninv;
            inv = inv & 1;
            inv.val[0] = (inv.val[0] + mod - 1) % mod;
            inv.reinit();
            REP(k, 0, inv.len - 1)
            inv.val[k] = (mod - inv.val[k]) % mod;
            inv = inv + x;
            inv.reserve(cur[j]);
            res = res * inv;
            res.reserve(cur[j]);
        }
        return res;
    }
    return x;
}

polynomial operator^(polynomial &x, int y) {
    static polynomial res;
    static int j;
    res = x;
    res = res && -1;
    res.reserve(x.len);
    REP(j, 0, res.len - 1)
    res.val[j] = ((long long)res.val[j] * y) % mod;
    res.reinit();
    res = res && 1;
    return res;
}

bool operator==(polynomial &x, polynomial &y) {
    if (x.len != y.len)
        return 0;
    static int i;
    REP(i, 0, x.len - 1)
    if (x.val[i] != y.val[i])
        return 0;
    return 1;
}

bool operator!=(polynomial &x, polynomial &y) { return !(x == y); }

int n, m;
int a[maxn];
polynomial p, q;

int main() {
    read(n);
    read(m);
    int i;
    REP(i, 0, n)
    read(a[i]);
    p.init(a);
    q.init(a);
    p = !p;
    p = ~p;
    p = p & 1;
    p = p && 1;
    p.reserve(n + 1);
    REP(i, 0, n)
    p.val[i] = (mod - p.val[i]) % mod;
    p.val[0] = (p.val[0] + 2) % mod;
    p = p + q;
    p.val[0] = (p.val[0] + mod - q.val[0]) % mod;
    p = p && -1;
    p.reserve(n + 1);
    p.val[0] = (p.val[0] + 1) % mod;
    p = p ^ m;
    p.reserve(n + 1);
    p = p & -1;
    p.output(1, 1);
    return 0;
}