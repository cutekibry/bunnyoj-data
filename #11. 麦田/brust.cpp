#include <algorithm>
#include <cstdio>

typedef long long int64;

const int MAXN = 15;
const int MOD = 998244353;

int n, k;
int ans;

int a[MAXN];
bool vis[MAXN];

void judge() {
    int i, x = 0;

    for (i = 1; i <= k; i++)
        x = std::max(x, a[i]);
    for (i = k + 1; i <= n; i++)
        if (a[i] > x) {
            ans += a[i];
            return;
        }
}
void dfs(int p) {
    int i;

    if (p > n) {
        judge();
        return;
    }
    for (i = 1; i <= n; i++) {
        if (!vis[i]) {
            vis[i] = true;
            a[p] = i;
            dfs(p + 1);
            vis[i] = false;
        }
    }
}

int main() {
    int T;

    scanf("%d", &T);
    while (T) {
        T--;
        scanf("%d %d", &n, &k);
        ans = 0;
        dfs(1);
        printf("%d\n", ans);
    }
    return 0;
}