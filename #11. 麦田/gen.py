#!/usr/bin/pypy3

from cyaron import IO, randint, ati
from math import sqrt
from os import system

N = ati([10] * 2 + [100] * 2 + [1000] * 2 + [1e5] * 10 + [1e6] * 2 + [1e7] * 2)
T = ati([1] + [100] * 3 + [1000] * 2 + [1e5] * 14)

for i in range(20):
    data = IO(file_prefix='field', data_id=i + 1)

    print('# generating field%d.in' % (i + 1))

    t = randint(max(1, T[i] - int(sqrt(T[i]))), T[i])
    data.input_writeln(t)
    n0 = randint(N[i] - int(sqrt(N[i])), N[i])
    for j in range(t):
        if i in [0, 6, 7, 8, 9]:
            n = n0
        else:
            n = randint(2, N[i])
        if i == 10:
            k = 1
        elif i == 11:
            k = randint(1, min(2, n - 1))
        elif i in [12, 13]:
            k = randint(1, min(100, n - 1))
        else:
            k = randint(1, n - 1)
        data.input_writeln(n, k)
    data.output_gen('./std')
    data.close()

    print('# validating field%d.in' % (i + 1))
    if system('./val < field%d.in' % (i + 1)) != 0:
        print('# field%d.in validating error' % (i + 1))
        exit(0)
