#include <cstdio>

typedef long long int64;

const int MAXN = 10000000 + 10;
const int MOD = 998244353;

int fac[MAXN], invfac[MAXN], inv[MAXN];

inline int qmod(int x) { return (x >= MOD) ? (x - MOD) : x; }
inline int qpow(int64 b, int p, int mod = MOD) {
    int res = 1;

    while (p) {
        if (p & 1)
            res = res * b % mod;
        b = b * b % mod;
        p >>= 1;
    }
    return res;
}
void initfac() {
    int i;

    fac[0] = invfac[0] = 1;
    for (i = 1; i < MAXN; i++)
        fac[i] = 1ll * fac[i - 1] * i % MOD;
    invfac[MAXN - 1] = qpow(fac[MAXN - 1], MOD - 2);
    for (i = MAXN - 2; i; i--)
        invfac[i] = invfac[i + 1] * (i + 1ll) % MOD;
    for (i = 1; i < MAXN; i++)
        inv[i] = 1ll * invfac[i] * fac[i - 1] % MOD;
}
inline int dpow(int n, int k) { return 1ll * fac[n] * invfac[n - k] % MOD; }

int main() {
    int T;
    int n, k;
    int ans;

    initfac();

    scanf("%d", &T);
    while (T) {
        T--;
        scanf("%d %d", &n, &k);

        ans = (1ll * dpow(n, k + 1) * inv[k + 1] + (n + 1ll) * dpow(n - 1, k) % MOD * inv[k]) % MOD;
        ans = 1ll * ans * inv[2] % MOD * k % MOD * fac[n - k] % MOD;
        printf("%d\n", ans);
    }
    return 0;
}