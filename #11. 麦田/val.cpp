#include "testlib.h"

int main(int argc, char *argv[]) {
    registerValidation();

    int T, n, k;
    int i;

    T = inf.readInt(1, 100000, "T");
    inf.readEoln();

    for (i = 1; i <= T; i++) {
        n = inf.readInt(2, 10000000, format("n[%d]", i));
        inf.readSpace();
        k = inf.readInt(1, n - 1, format("k[%d]", i));
        inf.readEoln();
    }
    inf.readEof();
}