// Author: Tsukimaru Oshawott
// In the name of
//
//      _/_/      _/_/_/        _/_/_/    _/_/_/_/    _/    _/    _/_/_/_/
//    _/    _/    _/    _/    _/          _/          _/    _/    _/
//    _/_/_/_/    _/_/_/_/    _/          _/_/_/_/    _/    _/    _/_/_/_/
//    _/    _/    _/  _/      _/          _/          _/    _/          _/
//    _/    _/    _/    _/      _/_/_/    _/_/_/_/    _/_/_/_/    _/_/_/_/
//

#include <algorithm>
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <cmath>
#include <ctime>
#include <cassert>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>

#define For(i, l, r) for (i = int64(l); i <= int64(r); i++)
#define Fo(i, n) For(i, 1, n)
#define Rof(i, r, l) for (i = int64(r); i >= int64(l); i--)
#define Ro(i, n) Rof(i, n, 1)
#define clr(a) memset(a, 0, sizeof(a))
#define cpy(a, b) memcpy(a, b, sizeof(b))
#define fc(a, ch) memset(a, ch, sizeof(a))

typedef unsigned uint;
typedef unsigned long long uint64;
typedef long long int64;

#define T1 template <class A>
#define T2 template <class A, class B>
#define T3 template <class A, class B, class C>
#define T4 template <class A, class B, class C, class D>
inline void read(char &x) {
    do
        x = getchar();

    while (x <= ' ');
}
inline void read(char *s) {
    char ch;

    do
        ch = getchar();

    while (ch <= ' ');

    while (ch > ' ') {
        (*s) = ch;
        s++;
        ch = getchar();
    }

    (*s) = 0;
}
inline void read(std::string &s) {
    char ch;

    do
        ch = getchar();

    while (ch <= ' ');

    while (ch > ' ') {
        s.push_back(ch);
        ch = getchar();
    }
}
T1 inline void readint(A &x) {
    bool neg = false;
    char ch;

    do
        ch = getchar();

    while (ch <= ' ');

    if (ch == '-') {
        neg = true;
        ch = getchar();
    }

    x = 0;

    while (ch > ' ') {
        x = x * 10 + (ch & 15);
        ch = getchar();
    }

    if (neg)
        x = -x;
}
inline void read(int &x) { readint(x); }
inline void read(uint &x) { readint(x); }
inline void read(int64 &x) { readint(x); }
inline void read(uint64 &x) { readint(x); }

T2 inline void read(A &a, B &b) {
    read(a);
    read(b);
}
T3 inline void read(A &a, B &b, C &c) {
    read(a);
    read(b);
    read(c);
}
T4 inline void read(A &a, B &b, C &c, D &d) {
    read(a);
    read(b);
    read(c);
    read(d);
}
inline void writeln() { putchar('\n'); }
T1 inline void writeint(A x) {
    static char buf[20];
    int top = 0;

    if (!x) {
        putchar('0');
        return;
    }

    if (x < 0) {
        putchar('-');
        x = -x;
    }

    while (x) {
        buf[++top] = (x % 10) | 48;
        x /= 10;
    }

    while (top)
        putchar(buf[top--]);
}
inline void write(int x) { writeint(x); }
inline void write(uint x) { writeint(x); }
inline void write(int64 x) { writeint(x); }
inline void write(uint64 x) { writeint(x); }
inline void write(char ch) { putchar(ch); }
inline void write(char *s) {
    while (*s) {
        putchar(*s);
        s++;
    }
}
inline void write(const char *s) { printf(s); }
T1 inline void write_(A x) {
    write(x);
    putchar(' ');
}
T1 inline void writeln(A x) {
    write(x);
    putchar('\n');
}
T2 inline void write(A a, B b) {
    write_(a);
    write(b);
}
T2 inline void writeln(A a, B b) {
    write_(a);
    writeln(b);
}
T3 inline void writeln(A a, B b, C c) {
    write_(a);
    write_(b);
    writeln(c);
}
T4 inline void writeln(A a, B b, C c, D d) {
    write_(a);
    write_(b);
    write_(c);
    writeln(d);
}
#undef T1
#undef T2
#undef T3
#undef T4



const int N = 5e4 + 10;
const int SIZE = N * 20 * 20;
const int64 INFLL = int(1e9) * int64(1e9);

int n, m;
int a[N];
int num[N];
int lson[SIZE], rson[SIZE], nodecnt;
int64 sum[SIZE];
unsigned short cnt[SIZE];
int rt[N];

#define mid ((l + r) >> 1)
inline int lowbit(int x) { return x & -x; }
void ins(int &p, int l, int r, int x, int k) {
	if(!p) p = ++nodecnt;
	sum[p] += num[x] * k;
	cnt[p] += k;
	if(l == r) return;
	else if(x <= mid) ins(lson[p], l, mid, x, k);
	else ins(rson[p], mid + 1, r, x, k);
}
void add(int p, int x, int k) {
	for(; p; p^=lowbit(p))
		ins(rt[p], 1, n, x k);
}

int getcnt(int r) {
	int res = 0;
	for(; r<=n; r+=lowbit(r))
		res += cnt[rt[r]];
	return res;
}
int64 query(int r0, int k) {
	static int p[20];
	int pn = 0, l = 1, r = n;
	int i, x;
	int64 res = 0;

	for(; r0<=n; r0+=lowbit(r0))
		p[++pn] = rt[r0];
	
	while(l ^ r) {
		x = 0;
		Fo(i, pn) x += cnt[lson[p[i]]];
		if(k <= x) {
			Fo(i, pn) p[i] = lson[p[i]];
			r = mid;
		}
		else {
			Fo(i, pn) {
				res += sum[lson[p[i]]];
				p[i] = rson[p[i]];
			}
			l = mid + 1;
			k -= x;
		}
	}
	x = 0;
	Fo(i, pn) x += cnt[p[i]];
	return (x < k) ? INFLL : (res + 1LL * k * num[l]);
}
#undef mid

int main() {
	int i, k;
	int l, r, mid;
	
	read(n, m);
	Fo(i, n) 
		read(a[i]);
	discrete();
	
	Fo(i, n) add(i, a[i], 1);
	while(true) {
		l = 1;
		r = getcnt(1);
		while(l ^ r) {
			mid = (l + r) >> 1;
			if(query(1, mid) <= m)
				l = mid;
			else
				r = mid - 1;
		}
		
		i = 1;
		Ro(k, r) {
			l = i;
			r = n;
			while(l ^ r) {
				mid = (l + r) >> 1;
				if(query(mid, k) <= )
			}
		}
	}
}
