#include <algorithm>
#include <cstdio>

typedef long long int64;

const int MOD = 998244353;
const int INV2 = 499122177;

int POW10[19];

inline int f(int64 b10, int p) {
    int64 b, res = 0;

    b = 1;
    while (p) {
        if (p & 1)
            res = (res * b10 + b) % MOD;
        b = b * (b10 + 1) % MOD;
        b10 = b10 * b10 % MOD;
        p >>= 1;
    }
    return res;
}

int main() {
    int n, k;
    int i, l, r;
    int res = 0;
    int modl, modr;

    POW10[0] = 1;
    for (i = 1; i <= 9; i++)
        POW10[i] = POW10[i - 1] * 10ll % MOD;

    scanf("%d %d", &n, &k);
    l = 1;
    r = 10;
    for (i = 1; l <= n; i++) {
        r = std::min(r, n + 1);

        modl = l % MOD;
        modr = r % MOD;
        res = (res + (modl + modr - 1ll) * (modr - modl) % MOD * INV2 % MOD * f(POW10[i], k)) % MOD;

        l = r;
        r *= 10;
    }
    printf("%d\n", res);
    return 0;
}