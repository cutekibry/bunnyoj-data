#!/usr/bin/python3
from cyaron import IO, randint, ati

N = ati([-1] + [100] + [1000]*2 + [1e9]*7)
K = ati([-1] + [100] + [1000]*2 + [1]*2 + [1e9]*5)

for i in range(1, 11):
    data = IO(file_prefix='', data_id=i)
    data.input_writeln(randint(1, N[i]), randint(1, K[i]))
    data.output_gen('./3000')
data.close()
