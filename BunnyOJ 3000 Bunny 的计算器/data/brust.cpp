#include <algorithm>
#include <cstdio>

typedef long long int64;

const int MOD = 998244353;
const int INV2 = 499122177;

int64 f(int n, int k) {
    int64 p10 = 1, res = 0;
    int i;

    while (p10 <= n)
        p10 *= 10;
    p10 = p10 % MOD;
    for (i = 1; i <= k; i++)
        res = (res * p10 + n) % MOD;
    return res;
}

int main() {
    int n, k, res = 0, i;
    scanf("%d %d", &n, &k);
    if (k == 1)
        printf("%lld\n", (1ll + n) * n / 2 % MOD);
    else {
        for (i = 1; i <= n; i++)
            res = (res + f(i, k)) % MOD;
        printf("%d\n", res);
    }
    return 0;
}