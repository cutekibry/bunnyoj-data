#include "testlib.h"

const int V = (1 << 30) - 1;

int main(int argc, char *argv[]) {
    registerValidation();

    int n, q;
    int i, c, l;

    n = inf.readInt(1, 1e6, "n");
    inf.readSpace();
    q = inf.readInt(1, 1e6, "q");
    inf.readEoln();

    for (i = 1; i <= n; i++) {
        inf.readInt(1, 3, format("t[%d]", i));
        inf.readSpace();
        inf.readInt(0, V, format("a[%d]", i));
        inf.readEoln();
    }

    for (i = 1; i <= q; i++) {
        c = inf.readInt(1, 2, format("c[%d]", i));
        inf.readSpace();

        if (c == 1) {
            inf.readInt(1, n, format("p[%d]", i));
            inf.readSpace();
            inf.readInt(1, 3, format("u[%d]", i));
            inf.readSpace();
            inf.readInt(0, V, format("b[%d]", i));
        }
        else {
            l = inf.readInt(1, n, format("l[%d]", i));
            inf.readSpace();
            inf.readInt(l, n, format("r[%d]", i));
            inf.readSpace();
            inf.readInt(0, V, format("x[%d]", i));
        }
        inf.readEoln();
    }
    inf.readEof();
}