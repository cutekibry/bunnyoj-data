#!/usr/bin/python3

from cyaron import IO, randint

C = [1, 1, 1, 1] + [2] + [1, 1] + [2] + [1, 1] + \
    [2, 2] + [1, 1] + [2, 2] + [1, 1, 1, 1]
T = [[1, 3]] * 4 + [[3, 3]] * 3 + [[2, 2]] * 3 + [[1, 2]] * 4 + [[1, 3]] * 6
N = [90, 2990, 91, 2991, 99992] + [99990 +
                                   x for x in ([3, 3, 4, 5, 5] + [6, 6, 7, 7, 8] + [8, 9, 9])] + [1000000] * 2

FULL = (1 << 30) - 1

for t in range(20):
    print('#', t)

    data = IO(file_prefix='', data_id=t+1, output_suffix='.ans')

    n = q = N[t]
    data.input_writeln(n, q)
    for i in range(n):
        a = randint(T[t][0], T[t][1])
        if 3 in range(T[t][0], T[t][1]) and randint(0, 4):
            a = 3

        if a == 1:
            x = FULL - (1 << randint(0, 29)) - (1 << randint(0, 29))
            if not randint(0, 9):
                x -= (1 << randint(0, 29))
        elif a == 2:
            x = (1 << randint(0, 29)) + (1 << randint(0, 29))
            if not randint(0, 9):
                x += (1 << randint(0, 29))
        else:
            x = randint(0, FULL)

        x = min(x, FULL)
        x = max(x, 0)

        if t <= 1:
            x = randint(0, 1)

        data.input_writeln(a, x)

    for i in range(q):
        opt = randint(C[t], 2)
        if opt == 1:
            a = randint(T[t][0], T[t][1])
            if 3 in range(T[t][0], T[t][1] + 1) and randint(0, 4):
                a = 3

            if a == 1:
                x = FULL - (1 << randint(0, 29)) - (1 << randint(0, 29))
                if not randint(0, 9):
                    x -= (1 << randint(0, 29))
            elif a == 2:
                x = (1 << randint(0, 29)) + (1 << randint(0, 29))
                if not randint(0, 9):
                    x += (1 << randint(0, 29))
            else:
                x = randint(0, FULL)

            x = min(x, FULL)
            x = max(x, 0)

            if t <= 1:
                x = randint(0, 1)
            data.input_writeln(1, randint(1, n), a, x)
        else:
            l = randint(1, n)
            r = randint(1, n)
            if l > r:
                l, r = r, l
            x = randint(0, FULL if t > 1 else 1)
            data.input_writeln(2, l, r, x)

    print('# Run')
    data.output_gen('./machine')
    data.close()
"""

data = IO(file_prefix='sample', data_id=0, output_suffix='.ans')
n = q = 90
data.input_writeln(n, q)
for i in range(n):
	data.input_writeln(randint(1, 3), randint(0, (1 << 10) - 1))
for i in range(q):
	if randint(0, 1):
		data.input_writeln(1, randint(1, n), randint(1, 3), randint(0, (1 << 10) - 1))
	else:
		l = randint(1, n)
		r = randint(1, n)
		if l>r:
			l, r = r, l
		data.input_writeln(2, l, r, randint(0, (1<<10)-1))
data.output_gen('./machine')
data.close()
"""
