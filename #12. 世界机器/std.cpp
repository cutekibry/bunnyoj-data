#include <cstdio>

const int MAXN = 1000000 + 10;
const int FULL = (1 << 30) - 1;

int n, q;
int aand[MAXN], axor[MAXN];
int sand[MAXN << 2], sxor[MAXN << 2];

inline void read(int &x) {
	char ch;
	
	do ch=getchar();
	while(ch<'0' or ch>'9');
	x=0;
	while(ch>='0' and ch<='9') {
		x=(x<<3)+(x<<1)+(ch&15);
		ch=getchar();
	}
}
inline void read(int &a, int &b) {
	read(a);
	read(b);
}
inline void read(int &a, int &b, int &c, int &d) {
	read(a);
	read(b);
	read(c);
	read(d);
}
inline void writeln(int x) {
	static char buf[20];
	int top = 0;
	
	if(!x) {
		putchar('0');
		putchar('\n');
		return;
	}
	while(x) {
		top++;
		buf[top] = (x % 10) | 48;
		x /= 10;
	}
	while(top) {
		putchar(buf[top]);
		top--;
	}
	putchar('\n');
}

#define lp (p << 1)
#define rp (p << 1 | 1)
#define mid ((l + r) >> 1)
#define lson lp, l, mid
#define rson rp, mid + 1, r
void up(int p) {
	sand[p] = sand[lp] & sand[rp];
	sxor[p] = sxor[lp] & sand[rp] ^ sxor[rp];
}
void build(int p, int l, int r) {
	if(l == r) {
		sand[p] = aand[l];
		sxor[p] = axor[l];
	}
	else {
		build(lson);
		build(rson);
		up(p);
	}
}

void update(int p, int l, int r, int x, int a, int b) {
	if(l == r) {
		sand[p] = a;
		sxor[p] = b;
	}
	else {
		if(x <= mid)
			update(lson, x, a, b);
		else
			update(rson, x, a, b);
		up(p);
	}
}
void query(int p, int l, int r, int a, int b, int &x) {
	if(a <= l and r <= b) {
		x = x & sand[p] ^ sxor[p];
		return;
	}
	if(a <= mid)
		query(lson, a, b, x);
	if(b > mid)
		query(rson, a, b, x);
}
#undef lp
#undef rp
#undef mid
#undef lson
#undef rson

int main() {
	int i, t, x, y, z;
	int opt;
	
	read(n, q);
	for(i=1; i<=n; i++) {
		read(t, x);
		if(t == 1) {
			aand[i] = x;
			axor[i] = 0;
		}
		else if(t == 2) {
			aand[i] = FULL ^ x;
			axor[i] = x;
		}
		else if(t == 3) {
			aand[i] = FULL;
			axor[i] = x;
		}
	}
	
	build(1, 1, n);
	for(i=1; i<=q; i++) {
		read(opt, x, y, z);
		if(opt == 1) {
			if(y == 1) {
				y = z;
				z = 0;
			}
			else if(y == 2) {
				y = FULL ^ z;
				z = z;
			}
			else if(y == 3) {
				y = FULL;
				z = z;
			}
			update(1, 1, n, x, y, z);
		}
		else {
			query(1, 1, n, x, y, z);
			writeln(z);
		}
	}
	return 0;
}
