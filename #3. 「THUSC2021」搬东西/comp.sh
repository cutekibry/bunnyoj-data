#!/bin/bash

PROB=move

if [ "$1" == "pretest" ];
then
	echo packing pretest
	rm "${PROB} (pretest).zip"
	cp "problem (pretest).conf" problem.conf
	zip "${PROB} (pretest).zip" std.cpp problem.conf val.cpp ${PROB}[1-9].in ${PROB}[1-9].out ${PROB}10.in ${PROB}10.out download -r
	rm problem.conf
elif [ "$1" == "final" ];
then
	echo packing final
	rm "${PROB} (final).zip"
	cp "problem (final).conf" problem.conf
	zip "${PROB} (final).zip" std.cpp problem.conf val.cpp ${PROB}*.in ${PROB}*.out download -r
	rm problem.conf
else
	echo UNRECOGNIZED ARGUMENT
fi
