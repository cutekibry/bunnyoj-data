#!/usr/bin/pypy3

from array import array
from cyaron import IO, randint, ati
from math import sqrt
from sys import argv
from os import system


N = [10, 100, 3000, 50000, 50000]
M = ati([1e9, 1e9, 1e9, 10, 1e9])


def gen_random(N: int, V: int, k: int = 0):
    n = randint(N - int(sqrt(N)), N)
    m = randint(V - int(sqrt(V)), V)
    if k == 0:
        k = randint(1, n)
    lim = min(max(1, k * 2 * m // n), m)
    return (m, [randint(1, lim) for i in range(n)])


def gen_constructed(N: int, V: int, step: int):
    n = randint(N - int(sqrt(N)), N)
    m = randint(V - int(sqrt(V)), V)
    a = []
    x = V // 2
    for i in range(n):
        a.append(x)
        x = (x + randint(0, step) - 1) % m + 1
    return (m, a)

data_cnt = 0


def write(res: tuple):
    global data_cnt

    data_cnt += 1
    data = IO(file_prefix='move', data_id=data_cnt)

    print('# generating move%d.in' % data_cnt)
    m, a = res
    data.input_writeln(len(a), m)
    data.input_writeln(a)

    print('# generating move%d.out' % data_cnt)
    data.output_gen('./std')
    print('# generating move%d finished' % data_cnt)
    data.close()


def gen_pretests():
    for t in range(5):
        write(gen_random(N[t], M[t]))
        write(gen_random(N[t], M[t]))


def gen_final_tests():
    global data_cnt

    if data_cnt != 5:
        data_cnt = 5

    for t in range(5):
        print('# sub %d' % (t + 1))
        write(gen_random(N[t], M[t]))
        write(gen_random(N[t], M[t], N[t] // 2))
        write(gen_random(N[t], 10))
        write(gen_constructed(N[t], M[t], 5))
        write(gen_constructed(N[t], M[t], 100))
        write(gen_constructed(N[t], M[t], 1000000))



if __name__ == '__main__':
    if len(argv) == 1 or argv[1] not in ['pretest', 'final']:
        print('unrecognized argument')
    else:
        system('g++ std.cpp -o std -O3 -std=c++11')
        if argv[1] == 'pretest':
            gen_pretests()
        else:
            gen_final_tests()
