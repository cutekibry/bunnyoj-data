// Author: Tsukimaru Oshawott
// In the name of
//
//      _/_/      _/_/_/        _/_/_/    _/_/_/_/    _/    _/    _/_/_/_/
//    _/    _/    _/    _/    _/          _/          _/    _/    _/
//    _/_/_/_/    _/_/_/_/    _/          _/_/_/_/    _/    _/    _/_/_/_/
//    _/    _/    _/  _/      _/          _/          _/    _/          _/
//    _/    _/    _/    _/      _/_/_/    _/_/_/_/    _/_/_/_/    _/_/_/_/
//

#include <algorithm>
#include <cassert>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>

#define For(i, l, r) for (i = int64(l); i <= int64(r); i++)
#define Fo(i, n) For(i, 1, n)
#define Rof(i, r, l) for (i = int64(r); i >= int64(l); i--)
#define Ro(i, n) Rof(i, n, 1)
#define clr(a) memset(a, 0, sizeof(a))
#define cpy(a, b) memcpy(a, b, sizeof(b))
#define fc(a, ch) memset(a, ch, sizeof(a))

typedef unsigned uint;
typedef unsigned long long uint64;
typedef long long int64;

#define T1 template <class A>
#define T2 template <class A, class B>
#define T3 template <class A, class B, class C>
#define T4 template <class A, class B, class C, class D>
inline void read(char &x) {
    do
        x = getchar();

    while (x <= ' ');
}
inline void read(char *s) {
    char ch;

    do
        ch = getchar();

    while (ch <= ' ');

    while (ch > ' ') {
        (*s) = ch;
        s++;
        ch = getchar();
    }

    (*s) = 0;
}
inline void read(std::string &s) {
    char ch;

    do
        ch = getchar();

    while (ch <= ' ');

    while (ch > ' ') {
        s.push_back(ch);
        ch = getchar();
    }
}
T1 inline void readint(A &x) {
    bool neg = false;
    char ch;

    do
        ch = getchar();

    while (ch <= ' ');

    if (ch == '-') {
        neg = true;
        ch = getchar();
    }

    x = 0;

    while (ch > ' ') {
        x = x * 10 + (ch & 15);
        ch = getchar();
    }

    if (neg)
        x = -x;
}
inline void read(int &x) { readint(x); }
inline void read(uint &x) { readint(x); }
inline void read(int64 &x) { readint(x); }
inline void read(uint64 &x) { readint(x); }

T2 inline void read(A &a, B &b) {
    read(a);
    read(b);
}
T3 inline void read(A &a, B &b, C &c) {
    read(a);
    read(b);
    read(c);
}
T4 inline void read(A &a, B &b, C &c, D &d) {
    read(a);
    read(b);
    read(c);
    read(d);
}
inline void writeln() { putchar('\n'); }
T1 inline void writeint(A x) {
    static char buf[20];
    int top = 0;

    if (!x) {
        putchar('0');
        return;
    }

    if (x < 0) {
        putchar('-');
        x = -x;
    }

    while (x) {
        buf[++top] = (x % 10) | 48;
        x /= 10;
    }

    while (top)
        putchar(buf[top--]);
}
inline void write(int x) { writeint(x); }
inline void write(uint x) { writeint(x); }
inline void write(int64 x) { writeint(x); }
inline void write(uint64 x) { writeint(x); }
inline void write(char ch) { putchar(ch); }
inline void write(char *s) {
    while (*s) {
        putchar(*s);
        s++;
    }
}
inline void write(const char *s) { printf(s); }
T1 inline void write_(A x) {
    write(x);
    putchar(' ');
}
T1 inline void writeln(A x) {
    write(x);
    putchar('\n');
}
T2 inline void write(A a, B b) {
    write_(a);
    write(b);
}
T2 inline void writeln(A a, B b) {
    write_(a);
    writeln(b);
}
T3 inline void writeln(A a, B b, C c) {
    write_(a);
    write_(b);
    writeln(c);
}
T4 inline void writeln(A a, B b, C c, D d) {
    write_(a);
    write_(b);
    write_(c);
    writeln(d);
}
#undef T1
#undef T2
#undef T3
#undef T4

const int N = 5e4 + 10;
const int64 INFLL = int(1e9) * int64(1e9);

std::set<int> posset;
std::priority_queue<int> pos[N];
std::priority_queue<int, std::vector<int>, std::greater<int> > selection;

int n, m;
int a[N], num[N], numn;

void discrete() {
    int i;

    cpy(num, a);
    std::sort(num + 1, num + 1 + n);
    numn = std::unique(num + 1, num + 1 + n) - num - 1;
    Fo(i, n) a[i] = std::lower_bound(num + 1, num + 1 + numn, a[i]) - num;
}

int recstk[N], rectop;

int main() {
    int i, j;
    int wsum;
    int anscnt = 0;
    std::set<int>::iterator it, nxtit;

    read(n, m);
    Fo(i, n) read(a[i]);

    discrete();
    Fo(i, n) {
        posset.insert(a[i]);
        pos[a[i]].push(i);
    }

    while (!posset.empty()) {
        anscnt++;

        wsum = 0;
        for (it = posset.begin(); it != posset.end() and wsum + num[*it] <= m; it = nxtit) {
            nxtit = it;
            nxtit++;
            while (!pos[*it].empty() and wsum + num[*it] <= m) {
                i = pos[*it].top();
                pos[*it].pop();
                selection.push(i);
                wsum += num[*it];
            }
            if (pos[*it].empty())
                posset.erase(it);
        }

        while (!selection.empty()) {
            i = selection.top();
            selection.pop();
            while ((it = posset.upper_bound(a[i])) != posset.end() and wsum + num[*it] - num[a[i]] <= m) {
                if ((j = pos[*it].top()) < i) {
                    recstk[++rectop] = *it;
                    posset.erase(it);
                }
                else {
                    pos[*it].pop();
                    pos[a[i]].push(i);               
                    posset.insert(a[i]);
                    selection.push(j);
                    if (pos[*it].empty())
                        posset.erase(it);

                    wsum += num[*it] - num[a[i]];
                    break;
                }
            }
        }
        while (rectop) {
            i = recstk[rectop--];
            if (!pos[i].empty())
                posset.insert(i);
        }
    }
    writeln(anscnt);
    return 0;
}
