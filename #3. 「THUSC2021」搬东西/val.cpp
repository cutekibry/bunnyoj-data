#include "testlib.h"

int main(int argc, char *argv[]) {
    registerValidation();

    int n, m, i, x;

    n = inf.readInt(1, 50000, "n");
    inf.readSpace();
    m = inf.readInt(1, 1e9, "m");
    inf.readEoln();

    for (i = 1; i <= n; i++) {
        inf.readInt(1, m, format("a[%d]", i));
        if (i < n)
            inf.readSpace();
    }
    inf.readEoln();

    inf.readEof();
}