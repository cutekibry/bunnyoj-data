#!/usr/bin/pypy3

from cyaron import IO, randint, ati

N = ati([1e2] * 5 + [1e4] * 5)
P = ati([1e2] + [1e3] + [1e9] * 3 + [5e3] + [1e9] * 4)

def is_prime(n):
    i = 2
    while i * i <= n:
        if n % i == 0:
            return False
        i += 1
    return True

for i in range(10):
    data = IO(file_prefix='nbsgs', data_id = i + 1)
    print('generate', i + 1)

    n = randint(N[i] * 9 // 10, N[i])
    p = randint(P[i] * 9 // 10, P[i])
    while not is_prime(p):
        p -= 1
    
    if i == 0:
        p = 2
    
    data.input_writeln(n, p)
    for j in range(n):
        a = randint(1, p - 1)
        b = pow(a, randint(0, p - 2), p)
        data.input_writeln(a, b)
    print('run std')
    data.output_gen('./std')
    print('run finished')
    data.close()
