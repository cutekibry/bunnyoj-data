直接用单次询问 $O(\sqrt P)$ 的 BSGS 可以实现 $O(n\sqrt P)$，通过 $50\%$ 的数据。

考虑使用 nBSGS。当然，nBSGS 是用来处理**底数**不变的多组询问，对于这道题，我们需要稍微转换一下。

令 $g$ 为模 $P$ 同余系下的一个原根（由于 $P$ 是质数，故原根必定存在），$g^r \equiv a \pmod P, g^q \equiv b \pmod P$，则

$$
\begin{aligned}
    a^x &\equiv b &\pmod P \\
    \left(g^r\right)^x &\equiv g^q &\pmod P \\
    g^{rx} &\equiv g^q &\pmod P \\
    rx &\equiv q &\pmod {P - 1} \\
\end{aligned}
$$

可见这是一个同余方程，可使用 exgcd 求解。

问题转化为如何求 $2n$ 次以 $g$ 为底的离散对数，即给出 $n$，求 $x$ 使得 $g^x \equiv n \pmod P$。

设 $x = iB + j$。由 $g^{iB + j} \equiv n \pmod P$ 可得 $g^j \equiv n g^{-iB} \pmod P$。预处理出 $(g^j, j)$ 的二元对存入 Hash 表后，对每个询问枚举 $i$，在 Hash 表中查找即可。

可见 $0 \leq j < B$，$0 \leq i < \frac {P - 1}B$，故复杂度为 $O(B + n\frac PB)$，取 $B = \sqrt {nP}$ 时得最优 $O(\sqrt {nP})$。

时间复杂度：$O(\sqrt {nP})$。

注意可能需要特判 $P = 2$ 的情况。