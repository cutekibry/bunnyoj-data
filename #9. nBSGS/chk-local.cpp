#include "testlib.h"

typedef long long int64;

inline int qpow(int64 b, int p, int mod) {
    int res = 1;
    while (p) {
        if (p & 1)
            res = res * b % mod;
        b = b * b % mod;
        p >>= 1;
    }
    return res;
}
bool is_prime(int n) {
    int i;

    for (i = 2; i * i <= n; i++)
        if (!(n % i))
            return false;
    return true;
}

int main(int argc, char *argv[]) {
    registerTestlibCmd(argc, argv);

    int n, P, a, b, x;

    n = inf.readInt(1, 1e4, "n");
    inf.readSpace();
    P = inf.readInt(2, 1e9, "P");
    ensuref(is_prime(P), "P is not prime");
    inf.readEoln();

    for (int i = 1; i <= n; i++) {
        a = inf.readInt(1, P - 1, format("a[%d]", i));
        inf.readSpace();
        b = inf.readInt(1, P - 1, format("b[%d]", i));
        inf.readEoln();

        x = ouf.readInt(0, P - 2, format("x[%d]", i));
        ouf.readEoln();

        ensuref(qpow(a, x, P) == b, "x[%d] is invalid", i);
    }
    quitf(_ok, "valid answer");
}