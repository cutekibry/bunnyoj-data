// Author: Tsukimaru Oshawott
// In the name of
//
//      _/_/      _/_/_/        _/_/_/    _/_/_/_/    _/    _/    _/_/_/_/
//    _/    _/    _/    _/    _/          _/          _/    _/    _/
//    _/_/_/_/    _/_/_/_/    _/          _/_/_/_/    _/    _/    _/_/_/_/
//    _/    _/    _/  _/      _/          _/          _/    _/          _/
//    _/    _/    _/    _/      _/_/_/    _/_/_/_/    _/_/_/_/    _/_/_/_/
//

#include "testlib.h"
#include <algorithm>
#include <bitset>
#include <cassert>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>

#define For(i, l, r) for (i = int64(l); i <= int64(r); i++)
#define Fo(i, n) For(i, 1, n)
#define Rof(i, r, l) for (i = int64(r); i >= int64(l); i--)
#define Ro(i, n) Rof(i, n, 1)
#define It(it, a) for (it = a.begin(); it != a.end(); it++)
#define Ti(it, a) for (it = a.rbegin(); it != a.rend(); it++)
#define clr(a) memset(a, 0, sizeof(a))
#define cpy(a, b) memcpy(a, b, sizeof(b))
#define fc(a, ch) memset(a, ch, sizeof(a))

typedef unsigned uint;
typedef unsigned long long uint64;
typedef long long int64;

#define T1 template <class A>
#define T2 template <class A, class B>
#define T3 template <class A, class B, class C>
#define T4 template <class A, class B, class C, class D>
inline void read(char &x) {
    do
        x = getchar();

    while (x <= ' ');
}
inline void read(char *s) {
    char ch;

    do
        ch = getchar();

    while (ch <= ' ');

    while (ch > ' ') {
        (*s) = ch;
        s++;
        ch = getchar();
    }

    (*s) = 0;
}
inline void read(std::string &s) {
    char ch;

    do
        ch = getchar();

    while (ch <= ' ');

    while (ch > ' ') {
        s.push_back(ch);
        ch = getchar();
    }
}
T1 inline void readint(A &x) {
    bool neg = false;
    char ch;

    do
        ch = getchar();

    while (ch <= ' ');

    if (ch == '-') {
        neg = true;
        ch = getchar();
    }

    x = 0;

    while (ch > ' ') {
        x = x * 10 + (ch & 15);
        ch = getchar();
    }

    if (neg)
        x = -x;
}
inline void read(int &x) { readint(x); }
inline void read(uint &x) { readint(x); }
inline void read(int64 &x) { readint(x); }
inline void read(uint64 &x) { readint(x); }

T2 inline void read(A &a, B &b) {
    read(a);
    read(b);
}
T3 inline void read(A &a, B &b, C &c) {
    read(a);
    read(b);
    read(c);
}
T4 inline void read(A &a, B &b, C &c, D &d) {
    read(a);
    read(b);
    read(c);
    read(d);
}
inline void writeln() { putchar('\n'); }
T1 inline void writeint(A x) {
    static char buf[20];
    int top = 0;

    if (!x) {
        putchar('0');
        return;
    }

    if (x < 0) {
        putchar('-');
        x = -x;
    }

    while (x) {
        buf[++top] = (x % 10) | 48;
        x /= 10;
    }

    while (top)
        putchar(buf[top--]);
}
inline void write(int x) { writeint(x); }
inline void write(uint x) { writeint(x); }
inline void write(int64 x) { writeint(x); }
inline void write(uint64 x) { writeint(x); }
inline void write(char ch) { putchar(ch); }
inline void write(char *s) {
    while (*s) {
        putchar(*s);
        s++;
    }
}
inline void write(const char *s) { printf("%s", s); }
T1 inline void write_(A x) {
    write(x);
    putchar(' ');
}
T1 inline void writeln(A x) {
    write(x);
    putchar('\n');
}
T2 inline void write(A a, B b) {
    write_(a);
    write(b);
}
T2 inline void writeln(A a, B b) {
    write_(a);
    writeln(b);
}
T3 inline void writeln(A a, B b, C c) {
    write_(a);
    write_(b);
    writeln(c);
}
T4 inline void writeln(A a, B b, C c, D d) {
    write_(a);
    write_(b);
    write_(c);
    writeln(d);
}
#undef T1
#undef T2
#undef T3
#undef T4

/*

$$a^x \equiv b \pmod P$$

令 $r = \text{ind}(a), q = \text{ind}(b)$，则

$$
\begin{aligned}
    \left(g^r\right)^x &\equiv g^q &\pmod P \\
    g^{rx} &\equiv g^q &\pmod P \\
    rx &\equiv q &\pmod {P - 1} \\
\end{aligned}
$$

可以使用 exgcd 求解。

*/

const int N = 4e6 + 10;

int n, P, g, B;
int invgb;

// Hash 表
namespace ds {
    const int HMOD = 19260817;
    int pre[N], key[N], head[HMOD], nodecnt;

    inline int get(int k) {
        int p = head[k % HMOD];
        while (p and key[p] ^ k)
            p = pre[p];
        return p - 1;
    }
    inline bool ins(int k) {
        if (get(k) != -1)
            return false;
        pre[++nodecnt] = head[k % HMOD];
        head[k % HMOD] = nodecnt;
        key[nodecnt] = k;
        return true;
    }
}; // namespace ds

inline int qpow(int64 b, int p, int mod) {
    int res = 1;
    while (p) {
        if (p & 1)
            res = res * b % mod;
        b = b * b % mod;
        p >>= 1;
    }
    return res;
}

// 求解 ax + by = gcd(a, b)
void exgcd(int a, int b, int &x, int &y, int &d) {
    if (!b) {
        x = 1;
        y = 0;
        d = a;
    }
    else {
        exgcd(b, a % b, y, x, d);
        y -= a / b * x;
    }
}
// 求解 ax = b (mod m)
// ax + my = b
bool cong(int a, int b, int m) {
    int x, y, d;
    exgcd(a, m, x, y, d);
    return b % d == 0;
}

// 求解原根
int getroot(int P) {
    static int d[30];
    int i, x, dn = 0, g;

    x = P - 1;
    for (i = 2; i * i <= x; i++)
        if (!(x % i)) {
            d[++dn] = (P - 1) / i;
            while (!(x % i))
                x /= i;
        }

    for (g = 2; g < P; g++) {
        Fo(i, dn) if (qpow(g, d[i], P) == 1) break;
        if (i > dn)
            return g;
    }
    ensuref(false, "validator error: getroot(%d) failed", P);
    return -1;
}

// 初始化 BSGS
// g^{iB + j} = x
// x * g^{-iB} = g^j
void init() {
    int i, x;

    B = std::min(P, int(sqrt(1LL * n * P)));
    g = getroot(P);
    invgb = qpow(qpow(g, P - 2, P), B, P);
    for (i = 0, x = 1; i < B and ds::ins(x); i++, x = 1LL * x * g % P)
        ;
}

// 求解 ind(x)
int ind(int x) {
    int i, t;

    for (i = 0; i < P; i += B, x = 1LL * x * invgb % P) {
        t = ds::get(x);
        if (t != -1)
            return i + t;
    }
    ensuref(false, "validator error: ind(%d) failed", x);
    return -1;
}
// 求解 a^x = b
inline bool solve(int a, int b) { return cong(ind(a), ind(b), P - 1); }

inline bool is_prime(int n) {
    int i;
    for (i = 2; i * i <= n; i++)
        if (!(n % i))
            return false;
    return true;
}

int main(int argc, char *argv[]) {
    registerValidation();
    int a, b;
    int i;

    n = inf.readInt(1, 1e4, "n");
    inf.readSpace();
    P = inf.readInt(2, 1e9, "P");
    ensuref(is_prime(P), "P is not prime");
    inf.readEoln();

    if (P > 2)
        init();

    Fo(i, n) {
        a = inf.readInt(1, P - 1, format("a[%d]", i));
        inf.readSpace();
        b = inf.readInt(1, P - 1, format("b[%d]", i));
        inf.readEoln();

        if (P > 2)
            ensuref(solve(a, b), "(a[%d]=%d, b[%d]=%d) has no valid solution", i, a, i, b);
    }

    inf.readEof();
}