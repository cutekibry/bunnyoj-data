#include "testlib.h"

int main(int argc, char *argv[]) {
    registerValidation();

    int n, i;

    n = inf.readInt(1, 23, "n");
    inf.readEoln();
    for (i = 1; i <= n; i++) {
        inf.readInt(1, 1e5, format("a[%d]", i));
        if (i < n)
            inf.readSpace();
    }
    inf.readEoln();
    inf.readEof();
}