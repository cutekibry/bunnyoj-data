#!/usr/bin/pypy3

from cyaron import IO, randint
from random import shuffle, randint
from os import system

V = int(1e5)


def gen_random(n, v):
    print(f"# random({n}, {v})")
    return (n, [randint(1, v) for i in range(n)])


def gen_constructed(n, v):
    print(f"# constructed({n}, {v})")
    return (n, [randint(1, 10) for i in range(n - 3)] + [v, v // 2, v // 4])


data_cnt = 0


def write(res: tuple):
    global data_cnt

    data_cnt += 1
    data = IO(file_prefix='record', data_id=data_cnt)

    print('# generating record%d.in' % data_cnt)

    n, a = res
    shuffle(a)
    data.input_writeln(n)
    data.input_writeln(a)

    # print('# generating record%d.out' % data_cnt)
    data.output_gen('./std')
    data.close()

    # print('# validate record%d.in' % data_cnt)
    if system('./val < record%d.in' % data_cnt) != 0:
        print('# !!! validate failed !!!')
        exit(1)


write(gen_random(5, 1 << 5))
write(gen_random(12, 1 << 12))
write(gen_random(17, V))
write(gen_random(20, V))
write(gen_random(23, V))

write(gen_constructed(18, V))
write(gen_constructed(20, V))
write(gen_constructed(21, V))
write(gen_constructed(22, V))
write(gen_constructed(23, V))
