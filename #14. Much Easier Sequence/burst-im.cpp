// Author: Tsukimaru Oshawott
// In the name of
//
//      _/_/      _/_/_/        _/_/_/    _/_/_/_/    _/    _/    _/_/_/_/
//    _/    _/    _/    _/    _/          _/          _/    _/    _/
//    _/_/_/_/    _/_/_/_/    _/          _/_/_/_/    _/    _/    _/_/_/_/
//    _/    _/    _/  _/      _/          _/          _/    _/          _/
//    _/    _/    _/    _/      _/_/_/    _/_/_/_/    _/_/_/_/    _/_/_/_/
//

#include <algorithm>
#include <bitset>
#include <cassert>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>

#define For(i, l, r) for (i = int64(l); i <= int64(r); i++)
#define Fo(i, n) For(i, 1, n)
#define Rof(i, r, l) for (i = int64(r); i >= int64(l); i--)
#define Ro(i, n) Rof(i, n, 1)
#define It(it, a) for (it = a.begin(); it != a.end(); it++)
#define Ti(it, a) for (it = a.rbegin(); it != a.rend(); it++)
#define clr(a) memset(a, 0, sizeof(a))
#define cpy(a, b) memcpy(a, b, sizeof(b))
#define fc(a, ch) memset(a, ch, sizeof(a))

typedef unsigned uint;
typedef unsigned long long uint64;
typedef long long int64;

#define T1 template <class A>
#define T2 template <class A, class B>
#define T3 template <class A, class B, class C>
#define T4 template <class A, class B, class C, class D>
inline void read(char &x) {
    do
        x = getchar();

    while (x <= ' ');
}
inline void read(char *s) {
    char ch;

    do
        ch = getchar();

    while (ch <= ' ');

    while (ch > ' ') {
        (*s) = ch;
        s++;
        ch = getchar();
    }

    (*s) = 0;
}
inline void read(std::string &s) {
    char ch;

    do
        ch = getchar();

    while (ch <= ' ');

    while (ch > ' ') {
        s.push_back(ch);
        ch = getchar();
    }
}
T1 inline void readint(A &x) {
    bool neg = false;
    char ch;

    do
        ch = getchar();

    while (ch <= ' ');

    if (ch == '-') {
        neg = true;
        ch = getchar();
    }

    x = 0;

    while (ch > ' ') {
        x = x * 10 + (ch & 15);
        ch = getchar();
    }

    if (neg)
        x = -x;
}
inline void read(int &x) { readint(x); }
inline void read(uint &x) { readint(x); }
inline void read(int64 &x) { readint(x); }
inline void read(uint64 &x) { readint(x); }

T2 inline void read(A &a, B &b) {
    read(a);
    read(b);
}
T3 inline void read(A &a, B &b, C &c) {
    read(a);
    read(b);
    read(c);
}
T4 inline void read(A &a, B &b, C &c, D &d) {
    read(a);
    read(b);
    read(c);
    read(d);
}
inline void writeln() { putchar('\n'); }
T1 inline void writeint(A x) {
    static char buf[20];
    int top = 0;

    if (!x) {
        putchar('0');
        return;
    }

    if (x < 0) {
        putchar('-');
        x = -x;
    }

    while (x) {
        buf[++top] = (x % 10) | 48;
        x /= 10;
    }

    while (top)
        putchar(buf[top--]);
}
inline void write(int x) { writeint(x); }
inline void write(uint x) { writeint(x); }
inline void write(int64 x) { writeint(x); }
inline void write(uint64 x) { writeint(x); }
inline void write(char ch) { putchar(ch); }
inline void write(char *s) {
    while (*s) {
        putchar(*s);
        s++;
    }
}
inline void write(const char *s) { printf("%s", s); }
T1 inline void write_(A x) {
    write(x);
    putchar(' ');
}
T1 inline void writeln(A x) {
    write(x);
    putchar('\n');
}
T2 inline void write(A a, B b) {
    write_(a);
    write(b);
}
T2 inline void writeln(A a, B b) {
    write_(a);
    writeln(b);
}
T3 inline void writeln(A a, B b, C c) {
    write_(a);
    write_(b);
    writeln(c);
}
T4 inline void writeln(A a, B b, C c, D d) {
    write_(a);
    write_(b);
    write_(c);
    writeln(d);
}
#undef T1
#undef T2
#undef T3
#undef T4

const int N = 23;
const int M = int(1e5) * N + 5;

int full;
int n;
int a[N];
int b[N], bn;
int checkcnt, checktime;
int allsum;

int pre[1 << N | 5], to[1 << N | 5], wdis[1 << N | 5], head[M], wcnt;

inline void addedge(int u, int v) {
    pre[++wcnt] = head[u];
    head[u] = wcnt;
    to[wcnt] = v;
}

#define lowbit(x) ((x) & (-x))
bool check(int s, int sum) {
    static int ts[1 << N];
    int i, f1;

    bn = 0;
    For(i, 0, n - 1) if (~s >> i & 1) b[bn++] = a[i];

    f1 = (1 << bn) - 1;
    checkcnt++;
    checktime += f1;

    For(i, 0, bn - 1) ts[1 << i] = b[i] % sum;
    Fo(i, f1) {
        ts[i] = ts[i ^ lowbit(i)] + ts[lowbit(i)] - sum;
        ts[i] = (ts[i] < 0) ? (ts[i] + sum) : ts[i];
        if (!ts[i])
            return true;
    }
    return false;
}

int main() {
    // #ifndef ONLINE_JUDGE
    //     freopen("record14.in", "r", stdin);
    //     freopen("std.out", "w", stdout);
    // #endif

    std::vector<int>::iterator it;
    int i, j, s, sum;
    int ans;

    read(n);
    full = (1 << n) - 1;
    For(i, 0, n - 1) {
        read(a[i]);
        allsum += a[i];
    }
    Fo(s, full - 1) {
        sum = 0;
        For(i, 0, n - 1) if (s >> i & 1) sum += a[i];
        if (sum <= allsum - sum)
            addedge(sum, s);
    }

    ans = allsum;
    Ro(i, allsum >> 1) {
        for (j = head[i]; j; j = pre[j]) {
            if (check(to[j], i)) {
                ans = allsum - i;
                break;
            }
        }
        if (j)
            break;
    }
    fprintf(stderr, "        # checkcnt = %d\n", checkcnt);
    fprintf(stderr, "        # checktime = %d\n", checktime);
    writeln(ans);
    return 0;
}