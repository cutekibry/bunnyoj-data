### 题目描述
给你一个长度为 $n$ 的序列，每次你可以取出两个数 $a_i,a_j$​ 并对它们做以下两种操作中的一种：

1. 将 $a_i+a_j$ 重新放回序列；
2. 将 $\operatorname{lcm}(a_i,a_j)$ 重新放回序列，此操作只可使用 $1$ 次。

如此操作直到序列最后只剩下一个数。

求出最终剩下的数字的最小值。

### 输入格式
第一行，一个整数 $n$，表示序列长度。

第二行，$n$ 个整数 $a_1, a_2, \ldots, a_n$，表示序列。

### 输出格式
只有一行，一个整数，表示答案。

### 样例
#### 样例输入 1
```plain
5
2 3 10 1 9
```

#### 样例输出 1
```plain
13
```

#### 样例解释 1
第一步，取出 $3, 9$，放回 $3 + 9 = 12$，序列 $a = \{2, 10, 1, 12\}$。

第二步，取出 $2, 10$，放回 $2 + 10 = 12$，序列 $a = \{1, 12, 12\}$。

第三步，取出 $12, 12$，放回 $\operatorname{lcm}(12,12)=12$，序列 $a = \{1, 12\}$。

第四步，取出 $1, 12$，放回 $1 + 12 = 13$，序列 $a = \{13\}$。

可以证明，$13$ 是可能的最小值。

#### 样例输入 2
```plain
3
5 7 11
```

#### 样例输出 2
```plain
23
```

#### 样例解释 2
第一步，取出 $5, 7$，放回 $5 + 7 = 12$，序列 $a = \{12, 11\}$。

第二步，取出 $12, 11$，放回 $12 + 11 = 23$，序列 $a = \{23\}$。

可以证明，$23$ 是可能的最小值。

### 数据范围与提示
对于 $100\%$ 的数据，$1 \leq n \leq 23$，$1 \leq a_i \leq 10^5$。

数据有一定梯度。

Idea：Hurrikale

Solution / Program / Data：Tsukimaru