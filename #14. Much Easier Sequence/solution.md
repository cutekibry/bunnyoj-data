对于任意正整数 $x \leq y$，不难注意到 $\operatorname{lcm}(x, y) < x + y$ 当且仅当 $x \mid y$，此时 $\operatorname{lcm}(x, y) = y$。

若 $x \nmid y$，则 $\operatorname{lcm}(x, y) \geq 2y \geq x + y$，就没有使用 $\operatorname{lcm}$ 操作的必要了。

转化为选两个互不相交的子集 $S, T$ 使得前者和为后者和的因子，且前者和最大。

直接子集枚举可以实现 $O(3^n)$。

利用折半搜索可以实现 $O((1 + \sqrt 2)^n + (\sum a_i))$。