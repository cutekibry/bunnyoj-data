#include "testlib.h"

int main(int argc, char *argv[]) {
    registerValidation();

    inf.readInt(0, 1000000000, "a");
    inf.readSpace();
    inf.readInt(0, 1000000000, "b");
    inf.readEoln();
    inf.readEof();
}