// In the name of
//
//      _/_/      _/_/_/        _/_/_/    _/_/_/_/    _/    _/    _/_/_/_/
//    _/    _/    _/    _/    _/          _/          _/    _/    _/
//    _/_/_/_/    _/_/_/_/    _/          _/_/_/_/    _/    _/    _/_/_/_/
//    _/    _/    _/  _/      _/          _/          _/    _/          _/
//    _/    _/    _/    _/      _/_/_/    _/_/_/_/    _/_/_/_/    _/_/_/_/
//

#include <cstdio>
#include <cassert>

#define For(i, l, r) for(int i=l; i<r; i++)
#define Fo(i, n) For(i, 0, n)

typedef unsigned long long uint64;

template<class T> inline void read(T &x) {
	char ch;
	
	do ch=getchar();
	while(ch<=' ');
	x=0;
	while(ch>' ') {
		x = x * 10 + (ch & 15);
		ch = getchar();
	}
}
template<class T> inline void writeln(T x) {
	static char buf[20];
	int top = 0;
	
	if(!x) 
		putchar('0');
	while(x) {
		buf[++top] = (x % 10) | 48;
		x /= 10;
	}
	while(top)
		putchar(buf[top--]);
	putchar('\n');
}


const int N = 1e5 + 10;

int n;
uint64 c[N], d[N];

inline void init(uint64 &c, uint64 &d, uint64 x) {
	c = x;
	Fo(i, 5)
		c *= 2 - c * x;
	d = -1ULL / x;
}

#define I(k) uint64 sum##k = 0
#define D(k) sum##k += ((c[i + k] * x <= d[i + k]) ? (c[i + k] * x) : 0)
#define C(k) sum += sum##k;

int main() {
	int q;
	uint64 x, sum;
	
	read(n);
	read(q);
	Fo(i, n) {
		read(x);
		assert(x & 1);
		init(c[i], d[i], x);
	}
	
	while(q--) {
		read(x);
		
		sum = 0;
		I(0); I(1); I(2); I(3);
		for(int i=0; i<n; i+=4) { D(0); D(1); D(2); D(3); }
		C(0); C(1); C(2); C(3);
		writeln(sum);
	}
	return 0;
}
