#!/usr/bin/python3

from cyaron import IO, randint, ati
from math import sqrt, gcd
from random import shuffle
from os import system


def lcm(x, y): return x // gcd(x, y) * y


N = [10] * 2 + [1000] * 2 + [5000] + [10000] + \
    [15000] + [20000] + [25000] + [30000]
Q = N
V = ati([100] + [1e18] + [1e5] + [1e18] + [1e18] * 6)

for t in range(10):
    print(t)
    data = IO(file_prefix='exactdiv', data_id=t + 1)

    n = randint(N[t] - int(sqrt(N[t])), N[t])
    q = randint(Q[t] - int(sqrt(Q[t])), Q[t])
    data.input_writeln(n, q)

    a = [randint(1, N[t] >> 1) << 1 | 1 for i in range(n // 2)] + \
        [randint(1, (V[t] - 1) >> 1) << 1 | 1 for i in range((n + 1) // 2)]

    a0 = a
    shuffle(a0)
    data.input_writeln(a0)

    for i in range(q):
        x = a[randint(n // 2, n - 1)]
        for k in range(5):
            y = a[randint(0, n // 2 - 1)]
            if lcm(x, y) <= V[t]:
                x = lcm(x, y)
        data.input_writeln(x)

    print('Running')
    data.output_gen('./std')
    data.close()

system('rm exactdiv.zip')
system('zip exactdiv.zip exactdiv*.in exactdiv*.out problem.conf std.cpp val.cpp')