#include "testlib.h"

int main(int argc, char *argv[]) {
    registerValidation();

    int n, q;
    int i;

    n = inf.readInt(1, 30000, "n");
    inf.readSpace();
    q = inf.readInt(1, 30000, "q");
    inf.readEoln();

    for (i = 1; i <= n; i++) {
        long long x = inf.readLong(1, 1000000000000000000LL, format("a[%d]", i));
        ensuref(x % 2 == 1, "a[%d](%lld) is even", i, x);
        if (i < n)
            inf.readSpace();
    }
    inf.readEoln();

    for (i = 1; i <= q; i++) {
        inf.readLong(1, 1000000000000000000LL, format("qx[%d]", i));
        inf.readEoln();
    }

    inf.readEof();
}