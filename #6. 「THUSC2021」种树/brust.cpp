// Author: Tsukimaru Oshawott
// In the name of
//
//      _/_/      _/_/_/        _/_/_/    _/_/_/_/    _/    _/    _/_/_/_/
//    _/    _/    _/    _/    _/          _/          _/    _/    _/
//    _/_/_/_/    _/_/_/_/    _/          _/_/_/_/    _/    _/    _/_/_/_/
//    _/    _/    _/  _/      _/          _/          _/    _/          _/
//    _/    _/    _/    _/      _/_/_/    _/_/_/_/    _/_/_/_/    _/_/_/_/
//

#include <algorithm>
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <cmath>
#include <ctime>
#include <cassert>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>

#define For(i, l, r) for (i = int64(l); i <= int64(r); i++)
#define Fo(i, n) For(i, 1, n)
#define Rof(i, r, l) for (i = int64(r); i >= int64(l); i--)
#define Ro(i, n) Rof(i, n, 1)
#define clr(a) memset(a, 0, sizeof(a))
#define cpy(a, b) memcpy(a, b, sizeof(b))
#define fc(a, ch) memset(a, ch, sizeof(a))

typedef unsigned uint;
typedef unsigned long long uint64;
typedef long long int64;
typedef __uint128_t uint128;

#define walk(i, u) for (i = head[u]; i; i = pre[i])

namespace encoding {
    int pre[150], to[150], head[75], deg[75], wcnt;
    uint128 res;

    inline void addedge(int u, int v) {
        pre[++wcnt] = head[u];
        head[u] = wcnt;
        to[wcnt] = v;
        deg[u]++;
    }
    void dfs2(int u) {
        int i;
        res = res * 3 + deg[u];
        walk(i, u) dfs2(to[i]);
    }
    void dfs65(int u) {
        int i;

        if (u ^ 1)
            res = res << 1 | 1;
        walk(i, u) dfs65(to[i]);
        if (u ^ 1)
            res = res << 1;
    }
    uint128 encode(int n, const int *p) {
        int i;

        clr(deg);
        clr(head);
        wcnt = res = 0;

        Rof(i, n, 2) addedge(p[i], i);
        Fo(i, n) if (deg[i] > 2) break;
        if (i > n)
            dfs2(1);
        else {
            dfs65(1);
            res = res << 1 | 1;
        }
        return res;
    }
}; // namespace encoding

namespace decoding {
    void decode2(int n, uint128 d, int *p) {
        static int sp[140], sk[140];
        int stop = 0;
        int i;
        uint128 p3;

        p3 = 1;
        Fo(i, n) p3 *= 3;
        Fo(i, n) {
            p3 /= 3;
            sk[stop]--;
            p[i] = sp[stop];

            stop++;
            sp[stop] = i;
            sk[stop] = d / p3 % 3;

            while (stop and !sk[stop])
                stop--;
        }
    }
    void decode65(int n, uint128 d, int *p) {
        static int stk[140];
        int stop = 1, idcnt = 1;
        int i;

        stk[1] = 1;
        p[1] = 0;
        Rof(i, 2 * n - 3, 0) {
            if (d >> i & 1) {
                idcnt++;
                p[idcnt] = stk[stop];
                stk[++stop] = idcnt;
            }
            else
                stop--;
        }
    }
    void decode(int n, uint128 d, int *p) {
        if (d & 1)
            decode2(n, d >> 1, p);
        else
            decode65(n, d, p);
    }
}; // namespace decoding

uint128 encode(int n, const int *p) { return encoding::encode(n, p); }
void decode(int n, uint128 d, int *p) { decoding::decode(n, d, p); }