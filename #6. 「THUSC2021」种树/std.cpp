// Author: Tsukimaru Oshawott
// In the name of
//
//      _/_/      _/_/_/        _/_/_/    _/_/_/_/    _/    _/    _/_/_/_/
//    _/    _/    _/    _/    _/          _/          _/    _/    _/
//    _/_/_/_/    _/_/_/_/    _/          _/_/_/_/    _/    _/    _/_/_/_/
//    _/    _/    _/  _/      _/          _/          _/    _/          _/
//    _/    _/    _/    _/      _/_/_/    _/_/_/_/    _/_/_/_/    _/_/_/_/
//

#include <algorithm>
#include <cassert>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>

#define For(i, l, r) for (i = int64(l); i <= int64(r); i++)
#define Fo(i, n) For(i, 1, n)
#define Rof(i, r, l) for (i = int64(r); i >= int64(l); i--)
#define Ro(i, n) Rof(i, n, 1)
#define clr(a) memset(a, 0, sizeof(a))
#define cpy(a, b) memcpy(a, b, sizeof(b))
#define fc(a, ch) memset(a, ch, sizeof(a))

typedef unsigned uint;
typedef unsigned long long uint64;
typedef long long int64;

/*
 f[n][k]: prefixsum=k，n 括号序列计数

 f[0][0] = 1

 f[n][k] = f[n - 1][k + 1] + f[n - 1][k - 1]
 */

typedef __uint128_t uint128;

const int N = 150;

uint128 f[N][N];
int pre[N], to[N], head[N], wcnt;
char a[N];
int an;

void init() {
    int n, k;
    f[0][0] = 1;
    Fo(n, N - 1) For(k, 0, n) f[n][k] = f[n - 1][k + 1] + (k ? f[n - 1][k - 1] : 0);
}

inline void addedge(int u, int v) {
    pre[++wcnt] = head[u];
    head[u] = wcnt;
    to[wcnt] = v;
}
void dfs(int u) {
    if (u ^ 1)
        a[++an] = '(';
    for (int i = head[u]; i; i = pre[i])
        dfs(to[i]);
    if (u ^ 1)
        a[++an] = ')';
}
uint128 encode(int n, const int *p) {
    int i, k;
    uint128 res = 1;

    if (!f[0][0])
        init();

    wcnt = 0;
    Fo(i, n) head[i] = 0;

    an = 0;
    Rof(i, n, 2) addedge(p[i], i);
    dfs(1);

    k = 0;
    Fo(i, an) {
        if (a[i] == ')')
            res += f[an - i][k + 1];
        k += (a[i] == '(') ? 1 : -1;
    }
    return res;
}
void decode(int n, uint128 d, int *p) {
    static int stk[N];
    int stop, idcnt;
    int i, k;

    if (!f[0][0])
        init();

    an = (n - 1) << 1;
    k = 0;
    Fo(i, an) {
        if (d <= f[an - i][k + 1]) {
            a[i] = '(';
            k++;
        }
        else {
            d -= f[an - i][k + 1];
            a[i] = ')';
            k--;
        }
    }

    stk[stop = 1] = idcnt = 1;
    p[1] = 0;
    Fo(i, an) {
        if (a[i] == '(') {
            idcnt++;
            p[idcnt] = stk[stop];
            stk[++stop] = idcnt;
        }
        else
            stop--;
    }
}