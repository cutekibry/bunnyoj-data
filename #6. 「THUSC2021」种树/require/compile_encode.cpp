#include "encode.h"
#include <cstdio>

namespace IO {
    const int B = 1 << 20;

    char inbuf[B], outbuf[B];
    int outcnt;

    inline char gc() {
        static char *p1 = inbuf, *p2 = inbuf;
        if (p1 == p2)
            p2 = inbuf + fread(p1 = inbuf, 1, B, stdin);
        return (p1 == p2) ? EOF : (*p1++);
    }
    inline void flush() {
        fwrite(outbuf, 1, outcnt, stdout);
        outcnt = 0;
    }
    inline void pc(char ch) {
        static int cnt = 0;
        if (outcnt == B)
            flush();
        outbuf[outcnt++] = ch;
    }

    inline __uint128_t read() {
        char ch;
        __uint128_t res = 0;

        do
            ch = gc();
        while (ch < '0' || ch > '9');
        while (ch >= '0' && ch <= '9') {
            res = res * 10 + ch - '0';
            ch = gc();
        }
        return res;
    }
    inline void write(__uint128_t x) {
        static char buf[20];
        int top = 0;

        if (x == 0)
            pc('0');
        while (x) {
            top++;
            buf[top] = (x % 10) + '0';
            x /= 10;
        }
        while (top) {
            pc(buf[top]);
            top--;
        }
    }
    inline void writeln() { pc('\n'); }
    inline void write_(__uint128_t x) {
        write(x);
        pc(' ');
    }
    inline void writeln(__uint128_t x) {
        write(x);
        pc('\n');
    }
}; // namespace IO

int main() {
    int p[75];
    int T, n;

    printf("MOONLIGHT\n");

    T = IO::read();
    IO::writeln(T);
    while (T--) {
        n = IO::read();
        IO::writeln(n);
        for (int i = 1; i <= n; i++) {
            p[i] = IO::read();
            IO::write_(p[i]);
        }
        IO::writeln();
        IO::writeln(encode(n, p));
    }
    IO::flush();
    return 0;
}