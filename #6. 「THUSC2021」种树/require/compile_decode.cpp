#include "decode.h"
#include <cstdio>

namespace IO {
    const int B = 1 << 20;

    char inbuf[B];

    inline char gc() {
        static char *p1 = inbuf, *p2 = inbuf;
        if (p1 == p2)
            p2 = inbuf + fread(p1 = inbuf, 1, B, stdin);
        return (p1 == p2) ? EOF : (*p1++);
    }

    inline __uint128_t read() {
        char ch;
        __uint128_t res = 0;

        do
            ch = gc();
        while (ch < '0' || ch > '9');
        while (ch >= '0' && ch <= '9') {
            res = res * 10 + ch - '0';
            ch = gc();
        }
        return res;
    }
}; // namespace IO

namespace checkequiv {
    int pre[300], to[300], head[300], wcnt;
    bool vis[300];
    bool iscyclic;

    void addedge(int u, int v) {
        pre[++wcnt] = head[u];
        head[u] = wcnt;
        to[wcnt] = v;
    }
    void predfs(int u) {
        if (vis[u])
            iscyclic = true;
        if (iscyclic)
            return;

        vis[u] = true;
        for (int i = head[u]; i; i = pre[i])
            predfs(to[i]);
    }

    bool compare(int r1, int r2) {
        int i1, i2;
        for (i1 = head[r1], i2 = head[r2]; i1 && i2; i1 = pre[i1], i2 = pre[i2])
            if (!compare(to[i1], to[i2]))
                return false;
        return !i1 && !i2;
    }

    bool check(int n, int *p0, int *p) {
        for (int i = 1; i <= n; i++) {
            head[i] = head[i + 123] = 0;
            vis[i] = vis[i + 123] = false;
        }
        wcnt = 0;

        if (p[1])
            return false;

        for (int i = n; i >= 2; i--) {
            if (p[i] < 1 || p[i] > n)
                return false;
            addedge(p0[i << 2], i);
            addedge(p[i] + 123, i + 123);
        }

        iscyclic = false;
        predfs(1);
        predfs(1 + 123);
        return !iscyclic && compare(1, 1 + 123);
    }
}; // namespace checkequiv

int main() {
    int p0[75 << 2], p[75];
    int T, n;
    __uint128_t d;

    printf("MOONLIGHT\n");

    T = IO::read();
    while (T--) {
        n = IO::read();
        for (int i = 1; i <= n; i++)
            p0[i << 2] = IO::read();
        d = IO::read();

        for (int i = 1; i <= n; i++)
            p[i] = -1;
        decode(n, d, p);

        if (!checkequiv::check(n, p0, p)) {
            printf("0\n");
            return 0;
        }
    }
    printf("100\n");
    return 0;
}