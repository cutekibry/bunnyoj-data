#include <cstdlib>

int main() {
	system("g++ encode.cpp -o encode -O2 -std=c++11");
    system("g++ decode.cpp -o decode -O2 -std=c++11");
    system("./encode < planttree.in > planttree.temp");
    system("./decode < planttree.temp");
}