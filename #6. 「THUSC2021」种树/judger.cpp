// Written by Itst (https://www.cnblogs.com/Itst/p/14009964.html)

#include <bits/stdc++.h>
#include "uoj_judger.h"
using namespace std;

string programme_name_A = "encode", programme_name_B = "decode";

vector<vector<int>> get_subtask_dependencies(int n) {
    vector<vector<int>> dependencies(n + 1, vector<int>());
    for (int t = 1; t <= n; t++) {
        if (conf_str("subtask_dependence", t, "none") == "many") {
            string cur = "subtask_dependence_" + vtos(t);
            int p = 1;
            while (conf_int(cur, p, 0) != 0) {
                dependencies[t].push_back(conf_int(cur, p, 0));
                p++;
            }
        }
        else if (conf_int("subtask_dependence", t, 0) != 0)
            dependencies[t].push_back(conf_int("subtask_dependence", t, 0));
    }
    return dependencies;
}

vector<int> subtask_topo_sort(int n, const vector<vector<int>> &dependencies) {
    priority_queue<int> Queue;
    vector<int> degree(n + 1, 0), sequence;
    for (int t = 1; t <= n; t++) {
        for (int x : dependencies[t])
            degree[x]++;
    }
    for (int t = 1; t <= n; t++) {
        if (!degree[t])
            Queue.push(t);
    }
    while (!Queue.empty()) {
        int u = Queue.top();
        Queue.pop();
        sequence.push_back(u);
        for (int v : dependencies[u]) {
            degree[v]--;
            if (!degree[v])
                Queue.push(v);
        }
    }
    reverse(sequence.begin(), sequence.end());
    return sequence;
}

// !!! NOTE HERE !!!
void compile(string name) {
    report_judge_status_f("compiling %s", name.c_str());
    string lang = conf_str("answer_language");
    if (lang.size() < 3 || string(lang, 0, 3) != "C++")
        end_judge_judgement_failed("Only C++ has been supported. Sorry for the inconvenience");
    string ver = lang == "C++" ? "-std=c++98" : "-std=c++11";
    RunCompilerResult comp = run_compiler(work_path.c_str(), "/usr/bin/g++", "-o", name.c_str(), "-x", "c++",
                                          ("compile_" + name + ".cpp").c_str(), "answer.code", "-lm", "-O2",
                                          "-DONLINE_JUDGE", ver.c_str(), NULL);
    if (!comp.succeeded)
        end_judge_compile_error(comp);
}

vector<vector<int>> subtask_dependencies;
vector<int> minscale;
int new_tot_time = 0;

PointInfo Judge_point(int id) {
    TestPointConfig tpc;
    tpc.auto_complete(id);
    string tempoutput = work_path + "/temp_output.txt";

    RunLimit lim = conf_run_limit(id, RL_DEFAULT);
    RunResult runA = run_submission_program(tpc.input_file_name, tempoutput, lim, programme_name_A);
    if (runA.type != RS_AC)
        return PointInfo(id, 0, -1, -1, "Programme A " + info_str(runA.type),
                         file_preview(tpc.input_file_name), file_preview(tempoutput), "");
    if (conf_has("token"))
        file_hide_token(tempoutput, conf_str("token", ""));

    RunResult runB = run_submission_program(tempoutput, tpc.output_file_name, lim, programme_name_B);
    if (runB.type != RS_AC)
        return PointInfo(id, 0, -1, -1, "Programme B " + info_str(runB.type), file_preview(tempoutput),
                         file_preview(tpc.output_file_name), "");
    if (runA.ust + runB.ust > lim.time * 1000)
        return PointInfo(id, 0, -1, -1, "Overall Time Limit Exceeded.", "", "", "");

    if (conf_has("token"))
        file_hide_token(tpc.output_file_name, conf_str("token", ""));
    RunCheckerResult chk_ret =
        run_checker(conf_run_limit("checker", id, RL_CHECKER_DEFAULT), conf_str("checker"),
                    tpc.input_file_name, tpc.output_file_name, tpc.answer_file_name);
    if (chk_ret.type != RS_AC) {
        return PointInfo(id, 0, -1, -1, "Checker " + info_str(chk_ret.type),
                         file_preview(tpc.input_file_name), file_preview(tpc.output_file_name), "");
    }

    return PointInfo(id, chk_ret.scr, runA.ust + runB.ust, max(runA.usm, runB.usm), "default",
                     file_preview(tpc.input_file_name), file_preview(tpc.output_file_name), chk_ret.info);
}

void Judge_subtask(int id) {
    vector<PointInfo> subtask_testinfo;
    int mn = 100;
    for (auto t : subtask_dependencies[id])
        mn = min(mn, minscale[t]);
    if (!mn) {
        minscale[id] = 0;
        add_subtask_info(id, 0, "Skipped", subtask_testinfo);
        return;
    }
    int from = conf_int("subtask_end", id - 1, 0), to = conf_int("subtask_end", id, 0),
        total = conf_int("subtask_score", id, 0);
    string statestr = "default";
    RunLimit currentlimit = conf_run_limit(id, RL_DEFAULT);
    for (int i = from + 1; i <= to; ++i) {
        report_judge_status_f(("Running on test " + to_string(i) + " on subtask " + to_string(id)).c_str());
        PointInfo res = Judge_point(i);
        subtask_testinfo.push_back(res);
        mn = min(mn, res.scr);
        new_tot_time = max(new_tot_time, res.ust);
        if (!mn) {
            statestr = res.info;
            break;
        }
    }
    minscale[id] = mn;
    add_subtask_info(id, 1.0 * mn / 100 * total,
                     (statestr == "default" ? (mn == 100 ? "Accepted" : "Acceptable Answer") : statestr),
                     subtask_testinfo);
}

void ordinary_test() {
    compile(programme_name_A);
    compile(programme_name_B);
    int num = conf_int("n_subtasks");
    subtask_dependencies = get_subtask_dependencies(num);
    minscale.resize(num + 1);
    vector<int> seq = subtask_topo_sort(num, subtask_dependencies);
    for (auto t : seq)
        Judge_subtask(t);
    tot_time = new_tot_time;
    bool alright = 1;
    for (int i = 1; i <= num; ++i)
        alright &= minscale[i] == 100;
    if (alright) {
        int m = conf_int("n_ex_tests");
        for (int i = 1; i <= m; i++) {
            report_judge_status_f("Judging Extra Test #%d", i);
            PointInfo po = Judge_point(-i);
            if (po.scr != 100) {
                po.num = -1;
                po.info = "Extra Test Failed : " + po.info + " on " + vtos(i);
                po.scr = -3;
                add_point_info(po);
                end_judge_ok();
            }
        }
        if (m != 0) {
            PointInfo po(-1, 0, -1, -1, "Extra Test Passed", "", "", "");
            add_point_info(po);
        }
        end_judge_ok();
    }
    end_judge_ok();
}

void custom_test() {
    end_judge_judgement_failed(
        "Custom test is unavailable for this problem. Sorry for the inconvenience."); // !!! NOTE HERE !!!

    report_judge_status_f("Compiling...");
    string langA = conf_str(programme_name_A + "_language"), langB = conf_str(programme_name_B + "_language");
    if (langA.size() < 3 || langB.size() < 3 || string(langA, 0, 3) != "C++" || string(langB, 0, 3) != "C++")
        end_judge_judgement_failed("Only C++ has been supported. Sorry for the inconvenience");
    langA = langA == "C++" ? "-std=c++98" : "-std=c++11";
    RunCompilerResult comp =
        run_compiler(work_path.c_str(), "/usr/bin/g++", "-o", "grader", "-x", "c++", "grader.cpp",
                     (programme_name_A + ".code").c_str(), (programme_name_B + ".code").c_str(), "-lm", "-O2",
                     "-DONLINE_JUDGE", langA.c_str(), NULL);
    if (!comp.succeeded)
        end_judge_compile_error(comp);
    report_judge_status_f("Judging...");
    CustomTestInfo res = ordinary_custom_test("grader");
    add_custom_test_info(res);
    end_judge_ok();
}

int main(int argc, char **argv) {
    judger_init(argc, argv);

    if (conf_is("custom_test", "on"))
        custom_test();
    else
        ordinary_test();

    return 0;
}