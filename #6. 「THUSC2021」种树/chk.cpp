#include "testlib.h"
#include <bits/stdc++.h>
using namespace std;
int main(int argc, char **argv) {
    registerTestlibCmd(argc, argv);
    int scr = ouf.readInt();
    if (scr == 100)
        quitf(_ok, "Accepted!");
    if (scr <= 0 || scr > 100)
        quitf(_wa, "Wrong Answer!");
    quitp(scr / 100., "Partially Correct?");
}
