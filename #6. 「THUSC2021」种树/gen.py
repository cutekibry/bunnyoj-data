#!/usr/bin/pypy3

from cyaron import IO, randint
from math import sqrt
from random import shuffle, random, uniform

T = [3000] * 2 + [100000] * 3 + [3000] * 2 + [100000] * 3
N = [64] + [65] * 3 + [70] + [65] + [70] * 4

for i in range(10):
    print(i)

    data = IO(file_prefix='planttree', data_id=i+1)
    t = T[i] - randint(0, 10)
    data.input_writeln(t)
    for j in range(t):
        if randint(0, 2):
            n = randint(64, N[i])
        else:
            n = randint(1, N[i])
        data.input_writeln(n)
        p = [0] * n
        dep = [1] * n
        deg = [0] * n

        C = [0] * 3

        if t <= 4:
            C[0] = 1
        else:
            if randint(0, 1):
                C[0] = uniform(0, 1)
                C[1] = uniform(0, 1 - C[0])
                C[2] = 1 - C[0] - C[1]
            else:
                C[0] = uniform(0.95, 1)
                C[1] = uniform(0, 1 - C[0])
                C[2] = 1 - C[0] - C[1]
            shuffle(C)

        u = 1
        while u < n:
            opt = random()
            if opt <= C[0]:
                p[u] = randint(0, u - 1)
            elif opt <= C[0] + C[1]:
                p[u] = randint(max(0, u - 2), u - 1)
            else:
                p[u] = randint(0, int(sqrt(u)) - 1)
            if (i % 5 == 2 and dep[p[u]] == 10) or (i % 5 == 3 and deg[p[u]] == 2):
                C[0] = 1
                continue

            deg[p[u]] += 1
            dep[u] = dep[p[u]] + 1
            u += 1

        for k in range(n):
            p[k] = p[k] + 1

        data.input_writeln(p)

    data.close()
