// Author: Tsukimaru Oshawott
// In the name of
//
//      _/_/      _/_/_/        _/_/_/    _/_/_/_/    _/    _/    _/_/_/_/
//    _/    _/    _/    _/    _/          _/          _/    _/    _/
//    _/_/_/_/    _/_/_/_/    _/          _/_/_/_/    _/    _/    _/_/_/_/
//    _/    _/    _/  _/      _/          _/          _/    _/          _/
//    _/    _/    _/    _/      _/_/_/    _/_/_/_/    _/_/_/_/    _/_/_/_/
//

#include <algorithm>
#include <cassert>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>

#define For(i, l, r) for (i = int64(l); i <= int64(r); i++)
#define Fo(i, n) For(i, 1, n)
#define Rof(i, r, l) for (i = int64(r); i >= int64(l); i--)
#define Ro(i, n) Rof(i, n, 1)
#define clr(a) memset(a, 0, sizeof(a))
#define cpy(a, b) memcpy(a, b, sizeof(b))
#define fc(a, ch) memset(a, ch, sizeof(a))

typedef unsigned uint;
typedef unsigned long long uint64;
typedef long long int64;

#define T1 template <class A>
#define T2 template <class A, class B>
#define T3 template <class A, class B, class C>
#define T4 template <class A, class B, class C, class D>
inline void read(char &x) {
    do
        x = getchar();

    while (x <= ' ');
}
inline void read(char *s) {
    char ch;

    do
        ch = getchar();

    while (ch <= ' ');

    while (ch > ' ') {
        (*s) = ch;
        s++;
        ch = getchar();
    }

    (*s) = 0;
}
inline void read(std::string &s) {
    char ch;

    do
        ch = getchar();

    while (ch <= ' ');

    while (ch > ' ') {
        s.push_back(ch);
        ch = getchar();
    }
}
T1 inline void readint(A &x) {
    bool neg = false;
    char ch;

    do
        ch = getchar();

    while (ch <= ' ');

    if (ch == '-') {
        neg = true;
        ch = getchar();
    }

    x = 0;

    while (ch > ' ') {
        x = x * 10 + (ch & 15);
        ch = getchar();
    }

    if (neg)
        x = -x;
}
inline void read(int &x) { readint(x); }
inline void read(uint &x) { readint(x); }
inline void read(int64 &x) { readint(x); }
inline void read(uint64 &x) { readint(x); }

T2 inline void read(A &a, B &b) {
    read(a);
    read(b);
}
T3 inline void read(A &a, B &b, C &c) {
    read(a);
    read(b);
    read(c);
}
T4 inline void read(A &a, B &b, C &c, D &d) {
    read(a);
    read(b);
    read(c);
    read(d);
}
inline void writeln() { putchar('\n'); }
T1 inline void writeint(A x) {
    static char buf[20];
    int top = 0;

    if (!x) {
        putchar('0');
        return;
    }

    if (x < 0) {
        putchar('-');
        x = -x;
    }

    while (x) {
        buf[++top] = (x % 10) | 48;
        x /= 10;
    }

    while (top)
        putchar(buf[top--]);
}
inline void write(int x) { writeint(x); }
inline void write(uint x) { writeint(x); }
inline void write(int64 x) { writeint(x); }
inline void write(uint64 x) { writeint(x); }
inline void write(char ch) { putchar(ch); }
inline void write(char *s) {
    while (*s) {
        putchar(*s);
        s++;
    }
}
inline void write(const char *s) { printf(s); }
T1 inline void write_(A x) {
    write(x);
    putchar(' ');
}
T1 inline void writeln(A x) {
    write(x);
    putchar('\n');
}
T2 inline void write(A a, B b) {
    write_(a);
    write(b);
}
T2 inline void writeln(A a, B b) {
    write_(a);
    writeln(b);
}
T3 inline void writeln(A a, B b, C c) {
    write_(a);
    write_(b);
    writeln(c);
}
T4 inline void writeln(A a, B b, C c, D d) {
    write_(a);
    write_(b);
    write_(c);
    writeln(d);
}
#undef T1
#undef T2
#undef T3
#undef T4

/*
 */

const bool PRETEST = true;

const int N[] = { -1, 10, 10, 14, 20, 20 };
const int M[] = { -1, 10, 200000, 200000, 1000000, 1000000 };

int n, m, full;

char fname[256];

inline int randint(int l, int r) {
    static unsigned seed = -1;
    seed ^= seed >> 5;
    seed ^= seed << 7;
    seed ^= seed >> 13;
    return seed % (r - l + 1) + l;
}
inline double randreal() { return randint(0, 1e9) / 1e9; }
inline bool check(double p) { return randreal() <= p; }

inline int B(int n, double p) {
    int cnt = 0, i;
    Fo(i, n) cnt += check(p);
    return cnt;
}

int main() {
    int subt, tid, id;
    int i, j, len, l, r;
    double p;

    id = PRETEST ? 0 : 5;
    Fo(subt, 5) {
        Fo(tid, PRETEST ? 1 : 3) {
            id++;
            fprintf(stderr, "# producing meal%d.in\n", id);
            sprintf(fname, "meal%d.in", id);
            freopen(fname, "w", stdout);
            n = N[subt] - randint(0, 1);
            m = M[subt] - randint(0, int(sqrt(M[subt])));
            writeln(n, m);
            p = PRETEST ? 0.5 : (0.2 + (tid - 1) / 2.0 * 0.6);

            if (subt == 4) {
                Fo(i, n) {
                    len = B(m, p);
                    l = randint(1, m - len + 1);
                    r = l + len - 1;
                    Fo(j, m) {
                        write((l <= j and j <= r) ? -1 : randint(0, 1e9));
                        if (j < m)
                            write(' ');
                    }
                    writeln();
                }
            }

            else {
                Fo(i, n) {
                    Fo(j, m) {
                        write(check(p) ? -1 : randint(0, 1e9));
                        if (j < m)
                            write(' ');
                    }
                    writeln();
                }
            }
            fclose(stdout);
            std::cout.clear();

            fprintf(stderr, "# producing meal%d.out\n", id);
            sprintf(fname, "./std < meal%d.in > meal%d.out", id, id);
            system(fname);
        }
    }
    return 0;
}
