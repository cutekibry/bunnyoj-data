#!/bin/bash

PROB=meal

if [ "$1" == "pretest" ];
then
	echo packing pretest
	rm "${PROB} (pretest).zip"
	cp "problem (pretest).conf" problem.conf
	zip "${PROB} (pretest).zip" std.cpp chk.cpp problem.conf ${PROB}[1-5].in ${PROB}[1-5].out download require -r
	rm problem.conf
elif [ "$1" == "final" ];
then
	echo packing final
	rm "${PROB} (final).zip"
	cp "problem (final).conf" problem.conf
	zip "${PROB} (final).zip" std.cpp chk.cpp problem.conf ${PROB}*.in ${PROB}*.out download require -r
	rm problem.conf
else
	echo UNRECOGNIZED ARGUMENT
fi