#include "testlib.h"

int main(int argc, char *argv[]) {
    registerValidation();

    int n, m, i, j;

    n = inf.readInt(1, 20, "n");
    inf.readSpace();
    m = inf.readInt(1, 1e6, "m");
    inf.readEoln();

    for (i = 1; i <= n; i++) {
        for (j = 1; j <= m; j++) {
            inf.readInt(-1, 1e9, format("a[%d][%d]", i, j));
            if (j < m)
                inf.readSpace();
        }
        inf.readEoln();
    }

    inf.readEof();
}