#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>

typedef long long int64;

const int N = 200000 + 10;

int n, k;
char s[N];
int h1[N], h2[N], P1[N], P2[N];
int MOD1, MOD2;
int B;

int64 a[N];

inline int randint(int l, int r) { return rand() % (r - l + 1) + l; }

inline int getrandp(int l, int r) {
    int p, i;

    p = randint(l, r);
    while (true) {
        for (i = 2; i * i <= p; i++)
            if (!(p % i))
                break;
        if (i * i <= p)
            p++;
        else
            break;
    }
    return p;
}
inline int64 gethash(int l, int r) {
    int x1 = (h1[r] + MOD1 - 1LL * h1[l - 1] * P1[r - l + 1] % MOD1) % MOD1;
    int x2 = (h2[r] + MOD2 - 1LL * h2[l - 1] * P2[r - l + 1] % MOD2) % MOD2;
    return 1LL * x1 * MOD2 + x2;
}

void init() {
    int i;

    srand(time(NULL));
    MOD1 = getrandp(9e8, 1e9);
    MOD2 = getrandp(9e8, 1e9);
    B = getrandp(50, 100);

    P1[0] = P2[0] = 1;
    for (i = 1; i <= n; i++)
        P1[i] = 1LL * P1[i - 1] * B % MOD1;
    for (i = 1; i <= n; i++)
        P2[i] = 1LL * P2[i - 1] * B % MOD2;
    for (i = 1; i <= n; i++)
        h1[i] = (1LL * h1[i - 1] * B + (s[i] - 'a')) % MOD1;
    for (i = 1; i <= n; i++)
        h2[i] = (1LL * h2[i - 1] * B + (s[i] - 'a')) % MOD2;
}

int main() {
    int i;

    scanf("%d %d", &n, &k);
    scanf("%s", s + 1);

    init();

    for (i = 1; i <= n - k + 1; i++)
        a[i] = gethash(i, i + k - 1);
    std::sort(a + 1, a + 1 + n - k + 1);
    printf("%ld\n", std::unique(a + 1, a + 1 + n - k + 1) - a - 1);
    return 0;
}